<?php

namespace App\Modules\UserManagement\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\UserManagement\Models\Role;

class RolesHierarchy extends Controller
{

    public function index()
    {
        $roleList = Role::getRoles();
        $roles = self::createTree($roleList, 0);
        dd($roleList);
        return view('user-management::roles.roles_hierarchy')->with(['roles' => $roles]);
    }

    public static function createTree(array $elements, $parentId = 0)
    {
        $return = [];
        foreach ($elements as $element) {
            if ($element['order'] == $parentId) {
                $children = self::createTree($elements, $element['en_id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $return[] = $element;
            }
        }
        return $return;
    }
}
