

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.add_update_role) {
    openRoleModal = function (roleId = '') {
        resetForm('#roleAddUpdateForm');
        if (roleId != '') {
            $.ajax({
                url: URL_ROLE_DETAILS,
                type: "POST",
                data: { _token: _token, role_id: roleId },
                success: function (result) {
                    if (result.status == 1) {
                        let data = result.data;
                        $("#addUpdateModalTitle").text('Update Role');
                        $("#roleId").val(data.en_id);
                        $("#roleDisplayName").val(data.role);
                        $("#roleModal").modal('show');
                    } else {
                        toast(result.heading, result.msg, 'warning');
                        actionButton.removeAttr('disabled');
                    }
                    loadingHide();
                }
            })

        } else {
            $("#addUpdateModalTitle").text('Add Role');
            $("#roleModal").modal('show');
        }
    }
}


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$("#roleManagementDataTable").DataTable({
    "pageLength": 25,
    "responsive": true,
    "serverSide": true,
    "ordering": true,
    "aaSorting": [],
    "processing": true,
    "order": [[0, "desc"]],
    "columnDefs": [
        { orderable: false, targets: [0, 4] },
        { "width": "5%", "targets": 0 },
        { "width": "70%", "targets": 1 },
        { "width": "10%", "targets": 2 },
        { "width": "8%", "targets": 3 },
        { "width": "7%", "targets": 4 }

    ],
    "language": {
        "searchPlaceholder": 'Search...',
        "sSearch": '',
        "infoFiltered": " ",
        'loadingRecords': '&nbsp;',
        'processing': dataTableLoader
    },
    "ajax": {
        "url": URL_ROLE_LIST,
        "type": "post",
        'data': function (data) {
            data._token = _token;
            return data;
        }
    },
    "AutoWidth": false,
    "columns": [
        { "data": "en_id" },
        { "data": "role", "name": "role" },
        { "data": "role_users_count", "name": "role_users_count" },
        { "data": "status", "name": "status" },
        { "data": "en_id" },
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) {
        loadingShow();
        var info = this.dataTable().api().page.info();
        var page = info.page;
        var length = info.length;
        var index = (page * length + (iDataIndex + 1));

        var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.role) + '","' + aData.status + '")';
        var editFunction = 'javascript:openRoleModal("' + aData.en_id + '")';
        var assignPermissionFunction = 'javascript:assignPermissionForRole("' + aData.en_id + '")';

        if (aData.status == 'active') {
            activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
            activeTitle = 'Inactivate';
            activeIcon = "la la-times-circle";
        } else if (aData.status == 'inactive') {
            activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
            activeTitle = 'Activate';
            activeIcon = "la la-check-circle";
        }

        var htmlData = '';
        htmlData += " <div class=\"dropdown dropdown-inline\">";
        htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        htmlData += "<i class=\"flaticon-more-1\"></i>";
        htmlData += "</button>";
        htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";

        if (ability.add_update_role)
            htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";

        if (ability.change_status_role)
            htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";

        if (ability.assign_role_permissions)
            htmlData += "<a class=\"dropdown-item\" href=" + assignPermissionFunction + "><i class=\"la la-key\"></i> Assign Permission</a>";

        htmlData += "</div>";
        htmlData += "</div>";


        $('td:eq(0)', nRow).html(index).addClass('text-center');
        $('td:eq(1)', nRow).html(aData.role).addClass('text-left');
        $('td:eq(2)', nRow).html(aData.role_users_count).addClass('text-center');
        $('td:eq(3)', nRow).html(activeStatus).addClass('text-center');
        $('td:eq(4)', nRow).html(htmlData).addClass('text-center');
    },
    "fnDrawCallback": function (oSettings) {
        var info = this.dataTable().api().page.info();
        var totalRecords = info.recordsDisplay;
        loadingHide();
        updateTotalRecordsCount("total-records-roles", totalRecords);
    }
});


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.change_status_role) {
    activateDeactivate = () => {
        var roleId = $("#activateUpdateId").val();
        $.ajax({
            type: 'POST',
            data: { _token: _token, role_id: roleId },
            url: URL_CHANGE_STATUS,
            success: function (data) {
                if (data.status == 1) {
                    sweetalert(data.heading, data.msg, 'success')
                    $('#roleManagementDataTable').DataTable().ajax.reload(null, false);
                } else {
                    sweetalert(data.heading, data.msg, 'error')
                }
            }
        });
    }
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.assign_role_permissions) {
    assignPermissionForRole = (roleId) => {
        window.location.href = URL_ASSIGN_ROLE_PERMISSION.replace(':roleId', roleId);
    }
}