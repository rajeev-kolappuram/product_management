<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'core'], function () {
    Route::get('/', function () {
        dd('This is the Core module index page. Build something great!');
    });
    Route::get('/filter-status-select-list', 'Core@getFilterStatusSelectList')->name('filter-status-select-list');
    Route::get('/test-mail', 'Core@testMail')->name('test-mail');
});
