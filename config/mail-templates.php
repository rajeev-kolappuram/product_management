<?php
return [
    'user_created' => [
        'subject' => 'Welcome to ' . config('app.name'),
        'message' => '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">We are all really excited to welcome you to our team!</span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><span style="color: #333333;"> </span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">Please click the below link to set your account password</span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><a href="[PASSWORD_UPDATE_LINK]" rel="noopener" style="text-decoration: underline;" target="_blank" title="Update your password">[PASSWORD_UPDATE_LINK]</a></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><span style="color: #333333;"> </span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">We are looking forward to working with you and seeing you achieve great things! Your insight is so important to us!</span></p>'
    ],
    'change_password' => [
        'subject' => 'Update your password',
        'message' => '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">You\'re receiving this e-mail because you requested a password reset for your account.</span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><span style="color: #333333;"> </span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">Please click the below link to set your account password</span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><a href="[PASSWORD_UPDATE_LINK]" rel="noopener" style="text-decoration: underline;" target="_blank" title="Update your password">[PASSWORD_UPDATE_LINK]</a></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; mso-line-height-alt: NaNpx; margin: 0;"><span style="color: #333333;"> </span></p>' .
            '<p style="line-height: 1.5; word-break: break-word; font-family: inherit; font-size: 16px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 16px; color: #333333;">We are looking forward to working with you and seeing you achieve great things! Your insight is so important to us!</span></p>'
    ]


];
