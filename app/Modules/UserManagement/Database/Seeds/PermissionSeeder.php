<?php

namespace App\Modules\UserManagement\Database\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
        |--------------------------------------------------------------------------
        | USER MANAGEMENT MODULE
        |--------------------------------------------------------------------------
        |
        | Mix provides a clean, fluent API for defining some Webpack build steps
        | for your Laravel application. By default, we are compiling the Sass
        | file for the application as well as bundling up all the JS files.
        |
        */
        // 1
        DB::table('permissions')->insert([
            'name' => 'usermanagement_module',
            'guard_name' => 'web',
            'parent' => 0,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 2
        DB::table('permissions')->insert([
            'name' => 'usermanagement_menu',
            'guard_name' => 'web',
            'parent' => 1,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 3
        /*
        |--------------------------------------------------------------------------
        | ROLES
        |--------------------------------------------------------------------------
        */
        DB::table('permissions')->insert([
            'name' => 'roles_sub_module',
            'guard_name' => 'web',
            'parent' => 1,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 4
        DB::table('permissions')->insert([
            'name' => 'roles_menu',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 5
        DB::table('permissions')->insert([
            'name' => 'view_role',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 6
        DB::table('permissions')->insert([
            'name' => 'add_update_role',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 7
        DB::table('permissions')->insert([
            'name' => 'change_status_role',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 8
        DB::table('permissions')->insert([
            'name' => 'assign_role_permissions',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 9
        DB::table('permissions')->insert([
            'name' => 'role_hierarchy',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 10
        DB::table('permissions')->insert([
            'name' => 'change_role_hierarchy',
            'guard_name' => 'web',
            'parent' => 3,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 11
        /*
        |--------------------------------------------------------------------------
        | PERMISSIONS
        |--------------------------------------------------------------------------
        */
        DB::table('permissions')->insert([
            'name' => 'permissions_sub_module',
            'guard_name' => 'web',
            'parent' => 1,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 12
        DB::table('permissions')->insert([
            'name' => 'permissions_menu',
            'guard_name' => 'web',
            'parent' => 11,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 13
        DB::table('permissions')->insert([
            'name' => 'view_permission',
            'guard_name' => 'web',
            'parent' => 11,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 14
        DB::table('permissions')->insert([
            'name' => 'add_update_permission',
            'guard_name' => 'web',
            'parent' => 11,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 15
        DB::table('permissions')->insert([
            'name' => 'change_status_permission',
            'guard_name' => 'web',
            'parent' => 11,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 16
        /*
        |--------------------------------------------------------------------------
        | USERS
        |--------------------------------------------------------------------------
        */
        DB::table('permissions')->insert([
            'name' => 'users_module',
            'guard_name' => 'web',
            'parent' => 0,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 17
        DB::table('permissions')->insert([
            'name' => 'users_menu',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 18
        DB::table('permissions')->insert([
            'name' => 'add_update_user',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 19
        DB::table('permissions')->insert([
            'name' => 'view_user',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 20
        DB::table('permissions')->insert([
            'name' => 'change_status_user',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 21
        DB::table('permissions')->insert([
            'name' => 'users_filter',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 22
        DB::table('permissions')->insert([
            'name' => 'users_filter_section',
            'guard_name' => 'web',
            'parent' => 21,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 23
        DB::table('permissions')->insert([
            'name' => 'users_role_filter',
            'guard_name' => 'web',
            'parent' => 21,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 24
        DB::table('permissions')->insert([
            'name' => 'users_department_filter',
            'guard_name' => 'web',
            'parent' => 21,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 25
        DB::table('permissions')->insert([
            'name' => 'users_status_filter',
            'guard_name' => 'web',
            'parent' => 21,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // 26
        DB::table('permissions')->insert([
            'name' => 'reset_password',
            'guard_name' => 'web',
            'parent' => 16,
            'status' => 'active',
            'created_by' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
