<?php

use Illuminate\Database\Seeder;

class OrderUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('model_has_roles')->insert([
            'role_id' => 7,
            'model_type' => 'App\Modules\Orders\Models',
            'model_id' => 6
        ]);
    }
}
