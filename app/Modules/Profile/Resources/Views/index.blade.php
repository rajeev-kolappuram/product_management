@extends('layouts.app')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <button class="kt-subheader__mobile-toggle kt-subheader__mobile-toggle--left" id="kt_subheader_mobile_toggle"><span></span></button>
                <h3 class="kt-subheader__title">{{ $profile_title }} </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('profile') }}" class="kt-subheader__breadcrumbs-link">Profile</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">{{ $profile_title }} </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
        <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
            @include('profile::profile_sidebar')
        </div>
        <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
            @yield('profile_content')
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    var URL_PROFILE_DETAILS = "{{ route('profile-details') }}";
</script>

<script type="text/javascript" src="{{ asset(mix('js/profile_user.js')) }}"></script>
@endsection