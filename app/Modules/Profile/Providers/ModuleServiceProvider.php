<?php

namespace App\Modules\Profile\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('profile', 'Resources/Lang', 'app'), 'profile');
        $this->loadViewsFrom(module_path('profile', 'Resources/Views', 'app'), 'profile');
        $this->loadMigrationsFrom(module_path('profile', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('profile', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('profile', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
