<?php

namespace App\Modules\Settings\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddUpdateZone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $zoneId = (Request::input('zone_id')) ? (Core::decodeId(Request::input('zone_id'))) : NULL;
        return [
            'zone_name' => ($zoneId) ? ('required|max:255|unique:zones,name,' . $zoneId . ',id') : ('required|max:255|unique:zones,name')
        ];
    }
}
