delay = 3000;
minimumResultsForSearch = 10;

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$(document).ajaxSend(function (event, request, settings) {
    if (settings.type == 'POST') {
        loadingShow();
    }
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$(document).ajaxComplete(function (event, request, settings) {
    if (settings.type == 'POST') {
        loadingHide();
    }
});

document.onreadystatechange = function () {
    if (document.readyState !== "complete") {
        loadingShow();
    } else {
        loadingHide();
    }
};


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$(function () {
    $(document).idleTimer(7200000);
    $(document).on("idle.idleTimer", function (event, elem, obj) {
        window.location.reload();
    });
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
loadingHide = () => {
    $('.loader').hide();
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
loadingShow = () => {
    var loaderHtml = '<div class="loader">';
    loaderHtml += '<div class="loader-inner">';
    loaderHtml += '<div class="loader-line-wrap">';
    loaderHtml += '<div class="loader-line"></div>';
    loaderHtml += '</div>';
    loaderHtml += '<div class="loader-line-wrap">';
    loaderHtml += '<div class="loader-line"></div>';
    loaderHtml += '</div>';
    loaderHtml += '<div class="loader-line-wrap">';
    loaderHtml += '<div class="loader-line"></div>';
    loaderHtml += '</div>';
    loaderHtml += '<div class="loader-line-wrap">';
    loaderHtml += '<div class="loader-line"></div>';
    loaderHtml += '</div>';
    loaderHtml += '<div class="loader-line-wrap">';
    loaderHtml += '<div class="loader-line"></div>';
    loaderHtml += '</div>';
    loaderHtml += '</div>';
    loaderHtml += '</div>';
    loadingHide();
    $(loaderHtml).appendTo('body');
    $('.loader').css('height', $('html').height());
}



/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
generateValidationWarning = (form, errors) => {
    $('.parsley-error').removeClass('parsley-error');
    $('.parsley-errors-list').remove();
    $.each(errors, function (key, message) {
        console.log($(form).find("[name=" + `${key}` + "]"));
        if ($(form).find("[name=" + `${key}` + "]")) {
            if ($(form).find("[name=" + `${key}` + "]").data('parsley-errors-container')) {
                var errorContainer = $(form).find("[name=" + `${key}` + "]").data('parsley-errors-container');
                $(errorContainer).addClass('parsley-error');
                $(errorContainer).append("<ul class='parsley-errors-list filled' aria-hidden='false'><li>" + message.join(", ") + "</li></ul>");
            } else {
                $(form).find("[name=" + `${key}` + "]").addClass('parsley-error');
                $(form).find("[name=" + `${key}` + "]").after("<ul class='parsley-errors-list filled' aria-hidden='false'><li>" + message.join(", ") + "</li></ul>");
            }
        }
    });
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
cancelForm = (e) => {
    var form = $(e).closest("form");
    var formSelector = form.attr('id')
    var model = form.data('model');
    $('.parsley-error').removeClass('parsley-error');
    $('.parsley-errors-list').remove();
    if (formSelector) {
        $(formSelector).find('input:hidden[name!=_token]').val('');
        $(formSelector).find('input[type=tel]').intlTelInput(telInputOptions);
        $(formSelector).trigger("reset");
        $(formSelector + ' select').select2('val', [null]);
    }
    form.trigger("reset");
    form.parsley().reset();
    if (model != undefined && model != false) {
        $('#' + model).modal('hide');
    }
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
resetForm = (formSelector) => {
    $('.parsley-error').removeClass('parsley-error');
    $('.parsley-errors-list').remove();
    $(formSelector).find('input:hidden[name!=_token]').val('');
    $(formSelector).find('input[type=tel]').intlTelInput(telInputOptions);
    $(formSelector).trigger("reset");
    $(formSelector + ' select').select2('val', [null]);
    $(formSelector).parsley().reset();
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
changeStatus = (id, name, status) => {
    $("#activateUpdateIcon").removeClass();
    if (status == 'active') {
        $("#activateUpdateIcon").addClass('fa fa-times-circle status-modal-danger-icon');
        $("#activateUpdateAreYouSure").addClass('');
        $("#activateUpdateText").html('Do you want to deactivate ' + name);
        $("#activateUpdateId").val(id);
        $("#activateDeactivateModal").modal('show');
    } else {
        $("#activateUpdateIcon").addClass('fa fa-check-circle status-modal-success-icon');
        $("#activateUpdateAreYouSure").addClass('');
        $("#activateUpdateText").html('Do you want to activate ' + name);
        $("#activateUpdateId").val(id);
        $("#activateDeactivateModal").modal('show');
    }
}


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
toast = (heading, msg, type) => {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": delay,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    if (type == 'error' || type == 'warning') {
        heading = (heading) ? (heading) : 'Warning';
        toastr.error(msg, heading);
    }
    else if (type == 'hazard') {
        heading = (heading) ? (heading) : 'Warning';
        toastr.warning(msg, heading);
    } else {
        heading = (heading) ? (heading) : 'Success';
        toastr.success(msg, heading);
    }

}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
sweetalert = (heading, msg, type, reload = false, func = false) => {
    let timerInterval;
    type = (type) ? (type) : 'success';
    Swal.fire({
        title: heading,
        text: msg,
        type: type,
        timer: delay,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then(() => {
        if (func)
            func();

        if (reload)
            window.location.reload();
    });
}




/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
loginAgain = (heading, message) => {
    let timerInterval;
    Swal.fire({
        title: heading,
        html: message,
        timer: delay,
        timerProgressBar: true,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                Swal.getContent().querySelector('b').textContent = Swal.getTimerLeft()
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
            window.location.reload();
        }
    })
}



/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
createOrUpdate = (e) => {
    var form = $(e).closest("form");
    var validation = form.data('validation');
    var model = form.data('model');
    var formUrl = form.attr('action');
    var dataTable = form.data('data-table');
    var reloadDiv = form.data('div');
    var redirectUrl = form.data('redirect-url');
    var actionButton = $('.action-button');
    var formData = form.serialize();
    if (validation != false) {
        form.parsley({
            excluded: ':hidden'
        }).validate();
        if (!form.parsley().isValid()) {
            return false;
        }
    }
    loadingShow();
    disableButton(e);
    $.ajax({
        url: formUrl,
        type: "POST",
        data: formData,
        success: function (data) {
            loadingHide();
            if (data.status == 'validation-error') {
                generateValidationWarning(form, data.errors);
            } else if (data.status == 1) {
                let timerInterval;
                Swal.fire({
                    title: data.heading,
                    text: data.msg,
                    type: 'success',
                    timer: delay,
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        if (redirectUrl != undefined && redirectUrl != false) {
                            window.location = redirectUrl;
                        }
                        if (dataTable != undefined && dataTable != false) {
                            $('#' + dataTable).DataTable().ajax.reload(null, false);
                        }
                        if (model != undefined && model != false) {
                            $('#' + model).modal('hide');
                        }
                        if (reloadDiv != undefined && reloadDiv != false) {
                            var reloadDivs = reloadDiv.split(',');
                            $.each(reloadDivs, function (index, value) {
                                window[value]();
                            });
                        }
                        form.find("input[type=text], textarea").val("");
                        actionButton.removeAttr('disabled');
                    }
                })
            } else if (data.status == 0) {
                toast(data.heading, data.msg, 'error');
                actionButton.removeAttr('disabled');
            } else {
                toast('Something went wrong', 'Please contact ablity team', 'error');
                actionButton.removeAttr('disabled');
            }
            loadingHide();
            enableButton(e);
        },
        error: function () {
            actionButton.removeAttr('disabled');
            enableButton(e);
        }
    });

}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
createOrUpdateWithFile = (e) => {
    var form = $(e).closest("form");
    var validation = form.data('validation');
    var model = form.data('model');
    var formUrl = form.attr('action');
    var dataTable = form.data('data-table');
    var redirectUrl = form.data('redirect-url');
    var actionButton = $('.action-button');
    if (validation != false) {
        form.parsley({
            excluded: ':hidden'
        }).validate();
        if (!form.parsley().isValid()) {
            return false;
        }
    }
    loadingShow();
    actionButton.attr('disabled', 'true');
    disableButton(e);
    var formData = new FormData($('#' + form.attr('id'))[0]);
    $.ajax({
        url: formUrl,
        type: 'POST',
        data: formData,
        enctype: 'multipart/form-data',
        success: function (data) {
            if (data.status == 1) {
                toast(data.heading, data.msg, 'success');
                if (redirectUrl != undefined && redirectUrl != false) {
                    window.location = redirectUrl;
                }
                if (dataTable != undefined && dataTable != false) {
                    $('#' + dataTable).DataTable().ajax.reload(null, false);

                }
                if (model != undefined && model != false) {
                    $('#' + model).modal('hide');
                }
                actionButton.removeAttr('disabled');
                enableButton(e);
                form.find("input[type=text], textarea").val("");
                form.find("input[type=file]").val("");
            } else if (data.status == 0) {
                toast(data.heading, data.msg, "error");
                actionButton.removeAttr('disabled');
                enableButton(e);
            } else if (data.status == 2) {
                toast(data.heading, data.msg, "warning");
                if (data.modalId != '') {
                    $("#" + data.modalId).html(data.modalData);
                    $("#" + data.modalId).modal('show');
                }
                actionButton.removeAttr('disabled');
                enableButton(e);
            } else {
                toast('Something went wrong', 'Please contact ablity team', 'error');
                actionButton.removeAttr('disabled');
                enableButton(e);
            }
            loadingHide();
        },
        error: function () {
            loadingHide();
            toast('Something went wrong', 'Please contact ablity team', 'error');
            actionButton.removeAttr('disabled');
            enableButton(e);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

$(document).ajaxError(function (event, jqXHR, textStatus, exception) {
    var pageReload = (jqXHR.responseJSON.status == 'csrf-exception') ? true : false;
    ajaxErrorAlert('error', jqXHR.responseJSON.status, jqXHR.responseJSON.errors, pageReload);
    loadingHide();
});

if ($.fn.dataTable) {
    $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
        console.log(message);
    };
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
ajaxErrorAlert = (textStatus, title, message, pageReload = false) => {
    Swal.fire({
        type: textStatus,
        title: 'Be cool!! You have a ' + title,
        text: message,
        timer: delay,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
    }).then(() => {
        if (pageReload)
            location.reload();
    })
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
disableButton = (e) => {
    var actionButton = $(e);
    var actionFunction = actionButton.attr('onclick');
    actionButton.attr('onclick', '');
    actionButton.attr('data-func', actionFunction);
    var loadingClass = 'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light';
    actionButton.addClass(loadingClass);
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
enableButton = (e) => {
    var actionButton = $(e);
    var actionFunction = actionButton.attr('data-func');
    actionButton.attr('onclick', actionFunction);
    actionButton.attr('data-func', '');
    var loadingClass = 'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light';
    actionButton.removeClass(loadingClass);
}


window.dataTableLoader = "<div><div class=\"datatable-loader-container\"><span class=\"datatable-loader-container-box datatable-loader-container-one\"></span><span class=\"datatable-loader-container-box datatable-loader-container-two\"></span><span class=\"datatable-loader-container-box datatable-loader-container-three\"></span></div><div class=\"datatable-loader-text\">Sit back and Relax.. Its packing.. </div></div>";