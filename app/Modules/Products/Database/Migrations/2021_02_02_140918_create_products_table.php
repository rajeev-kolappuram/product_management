<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('description');
            $table->string('color');
            $table->float('size');
            $table->integer('stock');
            $table->float('price');
            $table->string('image');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps('');
        });
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
