<?php

namespace App\Modules\UserManagement\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('user-management', 'Resources/Lang', 'app'), 'user-management');
        $this->loadViewsFrom(module_path('user-management', 'Resources/Views', 'app'), 'user-management');
        $this->loadMigrationsFrom(module_path('user-management', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('user-management', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('user-management', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
