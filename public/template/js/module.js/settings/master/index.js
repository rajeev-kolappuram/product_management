$(document).ready(function () {
    $('#masterTables').DataTable({
        "responsive": true,
        "serverSide": true,
        "ordering": true,
        "aaSorting": [],
        "order": [[0, "desc"]],
        "columnDefs": [
            { "orderable": false, targets: [0, 4] },
            { "width": "10%", "targets": [0] },
            { "width": "25%", "targets": [1] },
            { "width": "35%", "targets": 1 },
            { "width": "15%", "targets": [3, 4] }
        ],
        "language": {
            "searchPlaceholder": 'Search...',
            "sSearch": '',
            "infoFiltered": " "
        },
        "ajax": {
            "url": URL_getMasterTables,
            "type": "post",
            'data': function (data) {
                data._token = _token;
                return data;
            },
        },
        "AutoWidth": false,
        'columns': [
            { 'data': 'en_id' },
            { 'data': 'name' },
            { 'data': 'description' },
            { 'data': 'status' },
            { 'data': 'en_id' }
        ],
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            var info = this.dataTable().api().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDataIndex + 1));
            var htmlData = '';
            $('td:eq(0)', nRow).html(index).addClass('text-center');
        }
    });
});