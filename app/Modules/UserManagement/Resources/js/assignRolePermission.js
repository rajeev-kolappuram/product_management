loadAssignPermission = () => {
    var roleId = $("#roleId").val();
    $.ajax({
        type: 'POST',
        data: { _token: _token, role_id: roleId },
        url: URL_ASSIGN_ROLE_PERMISSION,
        success: function (data) {
            if (data.status == 1) {
                $('#permissionHierarchy').html(data.html);
                var data = JSON.parse(data.data);
                loadJsTree(data);
            } else {
                sweetalert(data.heading, data.msg, 'error')
            }
        }
    });
}
loadAssignPermission();



/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.loadJsTree = (data) => {
    $('#assignPermission').jstree({
        'plugins': ["wholerow", "checkbox", "types"],
        'core': {
            "themes": {
                "responsive": true
            },
            'data': data
        },
        "types": {
            "default": {
                "icon": "fa fa-folder kt-font-warning"
            },
            "file": {
                "icon": "fa fa-folder  kt-font-warning"
            }
        },
    }).on('loaded.jstree', function () {
        $('#assignPermission').jstree('open_all');
    });
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
assignPermissionsForRole = () => {
    var selectedRole = $("#roleId").val();
    var selectedPermissions = $('#assignPermission').jstree("get_selected");
    console.log(selectedPermissions);
    $.ajax({
        type: 'POST',
        data: {
            _token: _token,
            role_id: selectedRole,
            permission_ids: selectedPermissions
        },
        url: URL_ASSIGN_PERMISSION_FOR_ROLE,
        success: function (data) {
            if (data.status == 1) {
                sweetalert(data.heading, data.msg, 'success', true, loadAssignPermission);
            } else {
                sweetalert(data.heading, data.msg, 'warning');
            }
        }
    });
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
confirmRolePermission = () => {
    $("#confirmationIcon").addClass('fa fa-question-circle status-modal-danger-icon');
    $("#confirmationTitle").html('Are You Sure?');
    $("#confirmationText").html('Do you want to assign the permissions');
    $("#confirmationModal").modal('show');
}