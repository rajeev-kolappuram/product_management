<?php

namespace App\Modules\Profile\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Profile\Models\LoginHistories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileOverview extends Controller
{
    public function indexAction(Request $request)
    {
        return view('profile::overview.index');
    }

    public function loadActiveLogins(Request $request)
    {
        try {
            $EN_USER_ID = Core::encodeId(Auth::id());
            $activeLogins = LoginHistories::getActiveLogins($EN_USER_ID, 'active');
            $html = view('profile::overview.active_logins')->with(['activeLogins' => $activeLogins])->render();
            return response()->json(['status' => 1, 'heading' => 'Active Logins', 'msg' => 'User active logins found!', 'html' => $html]);
        } catch (\PDOException $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        }
    }

    public function signoutAllActiveSessions(Request $request)
    {
        try {
            $EN_USER_ID = Core::encodeId(Auth::id());
            $data['password'] = $request->password;
            $data['email'] = Auth::user()->email;
            $rule['password'] = 'required|max:9';
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                $error = Core::validationMsg($validator);
                return response()->json(['status' => 0, 'heading' => 'Validation Warning', 'msg' => $error]);
            } else {
                if (Auth::attempt($data)) {
                    $logoutResult = Auth::logoutOtherDevices($data['password']);
                    if ($logoutResult) {
                        LoginHistoriesController::logoutUserFromAllSessions($EN_USER_ID);
                        LoginHistoriesController::saveHistory($request);
                        return response()->json(['status' => 1, 'heading' => 'Successful', 'msg' => 'All other sessions are removed.']);
                    } else {
                        return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Logout not successful.Please try again.']);
                    }
                } else {
                    return response()->json(['status' => 0, 'heading' => 'Invalid Password', 'msg' => 'Please enter valid password and try again.']);
                }
            }
        } catch (\PDOException $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        }
    }
}
