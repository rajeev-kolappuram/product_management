<?php

namespace App\Modules\Profile\Models;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LoginHistories extends Model
{
    protected $table = 'login_histories';
    protected $guarded = [];

    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getLogginedAtAttribute($value)
    {
        return Core::dateDisplayFormat($value);
    }

    public static function getActiveLogins($EN_USER_ID, $isActive = false)
    {
        $loginHistories = [];
        $browserToken = Session::get('BROWSER_TOKEN');
        if ($EN_USER_ID) {
            $USER_ID = Core::decodeId($EN_USER_ID);
            $loginHistories = LoginHistories::select('login_histories.id as en_id', 'login_histories.ip_address', 'login_histories.user_agent', 'login_histories.created_at as loggined_at', DB::raw("IF(login_histories.browser_token='" . $browserToken . "',true,false) as current_session"))
                ->where('user_id', $USER_ID);
            if ($isActive) {
                $loginHistories = $loginHistories->where('status', 'logined');
            }
            $loginHistories = $loginHistories->orderBy('id', 'desc')
                ->get()->toArray();
        }
        return $loginHistories;
    }
}
