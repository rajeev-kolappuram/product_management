$("#productManagementDataTable").DataTable({
    "pageLength": 25,
    "responsive": true,
    "serverSide": true,
    "ordering": true,
    "aaSorting": [],
    "processing": true,
    "order": [[0, "desc"]],
    "columnDefs": [
        { orderable: false, targets: [0, 9] },
        { "width": "5%", "targets": 0 },
        { "width": "23%", "targets": 1 },
        { "width": "23%", "targets": 2 },
        { "width": "7%", "targets": 3 },
        { "width": "7%", "targets": 4 },
        { "width": "7%", "targets": 5 },
        { "width": "7%", "targets": 6 },
        { "width": "7%", "targets": 7 },
        { "width": "7%", "targets": 8 },
        { "width": "7%", "targets": 9 },
    ],
    "language": {
        "searchPlaceholder": 'Search...',
        "sSearch": '',
        "infoFiltered": " ",
        'loadingRecords': '&nbsp;',
        'processing': dataTableLoader
    },
    "ajax": {
        "url": URL_PRODUCT_LIST,
        "type": "post",
        'data': function (data) {
            data._token = _token;
            return data;
        }
    },
    "AutoWidth": false,
    "columns": [
        { "data": "en_id" },
        { "data": "title", "name": "title" },
        { "data": "description", "name": "description" },
        { "data": "color", "name": "color" },
        { "data": "size", "name": "size" },
        { "data": "stock", "name": "stock" },
        { "data": "weight", "name": "weight" },
        { "data": "price", "name": "price" },
        { "data": "status", "name": "status" },
        { "data": "en_id" },
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) {
        loadingShow();
        var info = this.dataTable().api().page.info();
        var page = info.page;
        var length = info.length;
        var index = (page * length + (iDataIndex + 1));

        var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.title) + '","' + aData.status + '")';
        var editFunction = 'javascript:openProductModal("' + aData.en_id + '")';

        if (aData.status == 'active') {
            activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
            activeTitle = 'Inactivate';
            activeIcon = "la la-times-circle";
        } else if (aData.status == 'inactive') {
            activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
            activeTitle = 'Activate';
            activeIcon = "la la-check-circle";
        }

        var htmlData = '';
        htmlData += " <div class=\"dropdown dropdown-inline\">";
        htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        htmlData += "<i class=\"flaticon-more-1\"></i>";
        htmlData += "</button>";
        htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";
        htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";
        htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";
        htmlData += "</div>";
        htmlData += "</div>";

        $('td:eq(0)', nRow).html(index).addClass('text-center');
        $('td:eq(1)', nRow).html(aData.title).addClass('text-left');
        $('td:eq(2)', nRow).html(aData.description).addClass('text-left');
        $('td:eq(3)', nRow).html(aData.color).addClass('text-center');
        $('td:eq(4)', nRow).html(aData.size).addClass('text-center');
        $('td:eq(5)', nRow).html(aData.stock).addClass('text-center');
        $('td:eq(6)', nRow).html(aData.weight).addClass('text-center');
        $('td:eq(7)', nRow).html(aData.price).addClass('text-center');
        $('td:eq(8)', nRow).html(activeStatus).addClass('text-center');
        $('td:eq(9)', nRow).html(htmlData).addClass('text-center');
    },
    "fnDrawCallback": function (oProducts) {
        var info = this.dataTable().api().page.info();
        var totalRecords = info.recordsDisplay;
        loadingHide();
        updateTotalRecordsCount("total-records-products", totalRecords);
    }

});


openProductModal = function (productsId = '') {
    resetForm('#productsAddUpdateForm');

    if (productsId != '') {
        $.ajax({
            url: URL_PRODUCTS_DETAILS,
            type: "POST",
            data: { _token: _token, products_id: productsId },
            success: function (result) {
                console.log(result);
                if (result.status == 1) {
                    let data = result.data;
                    $("#addUpdateModalTitle").text('Update Product');
                    $("#productsId").val(data.en_id);
                    $("#Products_title").val(data.title);
                    $("#description").val(data.description);
                    $("#size").val(data.size);
                    $("#stock").val(data.stock);
                    $("#weight").val(data.weight);
                    $("#price").val(data.price);
                    $('#colorSelect').val(data.color).trigger('change');
                    $("#productsModal").modal('show');
                } else {
                    toast(result.heading, result.msg, 'warning');
                    actionButton.removeAttr('disabled');
                }
                loadingHide();
            }
        })

    } else {
        $("#addUpdateModalTitle").text('Add Products');
        $("#productsModal").modal('show');
    }

}


activateDeactivate = () => {
    var productsId = $("#activateUpdateId").val();
    $.ajax({
        type: 'POST',
        data: { _token: _token, products_id: productsId },
        url: URL_CHANGE_STATUS,
        success: function (data) {
            if (data.status == 1) {
                sweetalert(data.heading, data.msg, 'success')
                $('#productManagementDataTable').DataTable().ajax.reload(null, false);
            } else {
                sweetalert(data.heading, data.msg, 'error')
            }
        }
    });
}
$("#colorSelect").select2();

$("#category").select2();