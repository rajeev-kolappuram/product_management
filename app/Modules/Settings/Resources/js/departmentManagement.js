openDepartmentModal = function (departmentId = '') {
    resetForm('#departmentAddUpdateForm');
    if (departmentId != '') {
        $.ajax({
            url: URL_DEPARTMENT_DETAILS,
            type: "POST",
            data: { _token: _token, department_id: departmentId },
            success: function (result) {
                if (result.status == 1) {
                    let data = result.data;
                    $("#addUpdateModalTitle").text('Update Department');
                    $("#departmentId").val(data.en_id);
                    $("#departmentName").val(data.name);
                    $("#departmentModal").modal('show');
                } else {
                    toast(result.heading, result.msg, 'warning');
                    actionButton.removeAttr('disabled');
                }
                loadingHide();
            }
        })

    } else {
        $("#addUpdateModalTitle").text('Add Department');
        $("#departmentModal").modal('show');
    }

}

$("#departmentManagementDataTable").DataTable({
    "pageLength": 25,
    "responsive": true,
    "serverSide": true,
    "ordering": true,
    "aaSorting": [],
    "processing": true,
    "order": [[0, "desc"]],
    "columnDefs": [
        { orderable: false, targets: [0, 3] },
        { "width": "5%", "targets": 0 },
        { "width": "70%", "targets": 1 },
        { "width": "18%", "targets": 2 },
        { "width": "7%", "targets": 3 }

    ],
    "language": {
        "searchPlaceholder": 'Search...',
        "sSearch": '',
        "infoFiltered": " ",
        'loadingRecords': '&nbsp;',
        'processing': dataTableLoader
    },
    "ajax": {
        "url": URL_DEPARTMENT_LIST,
        "type": "post",
        'data': function (data) {
            data._token = _token;
            return data;
        }
    },
    "AutoWidth": false,
    "columns": [
        { "data": "en_id" },
        { "data": "name", "name": "name" },
        { "data": "status", "name": "status" },
        { "data": "en_id" },
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) {
        loadingShow();
        var info = this.dataTable().api().page.info();
        var page = info.page;
        var length = info.length;
        var index = (page * length + (iDataIndex + 1));

        var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.name) + '","' + aData.status + '")';
        var editFunction = 'javascript:openDepartmentModal("' + aData.en_id + '")';

        if (aData.status == 'active') {
            activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
            activeTitle = 'Inactivate';
            activeIcon = "la la-times-circle";
        } else if (aData.status == 'inactive') {
            activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
            activeTitle = 'Activate';
            activeIcon = "la la-check-circle";
        }

        var htmlData = '';
        htmlData += " <div class=\"dropdown dropdown-inline\">";
        htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        htmlData += "<i class=\"flaticon-more-1\"></i>";
        htmlData += "</button>";
        htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";
        htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";
        htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";
        htmlData += "</div>";
        htmlData += "</div>";


        $('td:eq(0)', nRow).html(index).addClass('text-center');
        $('td:eq(1)', nRow).html(aData.name).addClass('text-left');
        $('td:eq(2)', nRow).html(activeStatus).addClass('text-center');
        $('td:eq(3)', nRow).html(htmlData).addClass('text-center');
    },
    "fnDrawCallback": function (oSettings) {
        var info = this.dataTable().api().page.info();
        var totalRecords = info.recordsDisplay;
        loadingHide();
        updateTotalRecordsCount("total-records-departments", totalRecords);
    }

});

activateDeactivate = () => {
    var departmentId = $("#activateUpdateId").val();
    $.ajax({
        type: 'POST',
        data: { _token: _token, department_id: departmentId },
        url: URL_CHANGE_STATUS,
        success: function (data) {
            if (data.status == 1) {
                sweetalert(data.heading, data.msg, 'success')
                $('#departmentManagementDataTable').DataTable().ajax.reload(null, false);
            } else {
                sweetalert(data.heading, data.msg, 'error')
            }
        }
    });
}
