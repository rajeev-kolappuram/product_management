<?php

namespace App\Modules\Settings\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Settings\Http\Requests\AddUpdateDepartment;
use App\Modules\Settings\Models\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Departments extends Controller
{
    public function index()
    {
        return view('settings::departments.index');
    }

    public function addUpdateDepartment(AddUpdateDepartment $request)
    {
        try {
            $departmentId = (Request::input('department_id')) ? (Core::decodeId(Request::input('department_id'))) : NULL;
            $department = Request::input('department');
            $departmentData = [
                'name'        => Request::input('department_name'),
                'created_by'  => Auth::id()
            ];
            if ($departmentId) {
                $isDepartmentUpdated = Department::where('id', $departmentId)->update($departmentData);
                if ($isDepartmentUpdated) {
                    return response()->json(['status' => 1, 'heading' => 'Department Updated', 'msg' => $department  . ' updated succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $department  . ' was not updated succesfully.']);
            } else {
                $isDepartmentCreated = Department::create($departmentData);
                if ($isDepartmentCreated) {
                    return response()->json(['status' => 1, 'heading' => 'Department Created', 'msg' =>  $department  . ' created succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $department  . ' was not created succesfully.']);
            }
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getDepartmentList()
    {
        try {
            $limit = (Request::input('length') != '') ? Request::input('length') : 10;
            $offset = (Request::input('start') != '') ? Request::input('start') : 0;
            $search = Request::input('search');
            $order = Request::input('order');
            $columns = Request::input('columns');
            $colName = 'id';
            $sort = 'desc';
            if (isset($order[0]['column']) && isset($order[0]['dir'])) {
                $colNo = $order[0]['column'];
                $sort = $order[0]['dir'];
                if (isset($columns[$colNo]['name'])) {
                    $colName = $columns[$colNo]['name'];
                }
            }
            $filterData = [];
            $result = Department::getDepartmentsList($limit, $offset, $search, $colName, $sort, $filterData);
            $data = ["iTotalDisplayRecords" => $result['count'], "iTotalRecords" => $limit, "TotalDisplayRecords" => $limit];
            $data['data'] = $result['data'];
            return response()->json($data);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getDepartmentDetails()
    {
        try {
            $departmentId = (Request::input('department_id')) ? (Core::decodeId(Request::input('department_id'))) : NULL;
            if ($departmentId) {
                $departmentDetails = Department::getDepartmentDetails($departmentId);
                if ($departmentDetails) {
                    return response()->json(['status' => 1, 'heading' => 'Department Details', 'msg' => 'Department details found successfully', 'data' => $departmentDetails]);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Department details not found!.', 'data' => []]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Department details not found!.', 'data' => []]);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function changeStatus()
    {
        try {
            $departmentId = (Request::input('department_id')) ? (Core::decodeId(Request::input('department_id'))) : NULL;
            if ($departmentId) {
                $departmentDetails = Department::select('id', 'status')->where('id', $departmentId)->first();
                if ($departmentDetails) {
                    $status = ($departmentDetails['status'] == 'active') ? 'inactive' : 'active';
                    $updateStatus = Department::where('id', $departmentId)->update(['status' => $status]);
                    if ($updateStatus)
                        return response()->json(['status' => 1, 'heading' => 'Status Updated', 'msg' => 'Department status updated']);
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Department status not updated!']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Department details not found!']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Department details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }


    public function getDepartmentSelectList()
    {
        try {
            $parameters = collect();
            $parameters['search'] = Request::input('q');
            $departments = Department::getDepartmentSelecList($parameters);
            return $departments;
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }
}
