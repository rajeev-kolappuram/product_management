<?php

namespace App\Modules\Core\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Hashids;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class Core extends Controller
{
    public static function encodeId($id)
    {
        if (!empty($id)) {
            return HashIds::encode($id);
        }
    }

    public static function decodeId($decodedId)
    {
        if (!empty($decodedId)) {
            $decodedId = Hashids::decode($decodedId);
            if (count($decodedId) > 0) {
                return $decodedId[0];
            }
        }
    }

    public static function validationMsg($validator)
    {
        $error = "";
        foreach (array_values($validator->messages()->toArray()) as $msg) {
            $error = $error . implode(' ', $msg);
        }
        return $error;
    }

    public static function getTimeFormat()
    {

        return 'h:i A';
    }

    public static function getDateFormat()
    {
        return 'd/m/Y';
    }

    public static function getDateTimeFormat()
    {
        $dateFormat = 'd/m/Y h:i A';
        return $dateFormat;
    }

    public static function formatDate($date)
    {
        $dateFormat = self::getDateFormat();
        if ($date != '') {
            $date = str_replace('/', '-', $date);
            $date = Carbon::createFromFormat($dateFormat, $date)->format('Y-m-d');
            return $date;
        }
    }

    public static function formatDateTime($date)
    {
        $dateFormat = self::getDateTimeFormat();
        if ($date != '') {
            $date = Carbon::createFromFormat($dateFormat, $date)->format('Y-m-d H:i:s');
            return $date;
        }
    }

    public static function dateDisplayFormat($value, $formatType = 'date_time')
    {
        if ($value != '') {
            $date = $value;
            if ($formatType == 'date') {
                $format = self::getDateFormat();
                $date = Carbon::parse($value)->format($format);
            } else {
                $format = self::getDateTimeFormat();
                $date = Carbon::parse($value)->format($format);
            }
            return $date;
        }
    }

    public static function dbFormatDate($date)
    {
        $dateFormat = self::getDateFormat();
        if ($date != '') {
            $date = Carbon::createFromFormat($dateFormat, $date)->format('Y-m-d');
            return $date;
        }
    }

    public static function dbFormatDateTime($date)
    {
        $dateFormat = self::getDateTimeFormat();
        if ($date != '') {
            $date = Carbon::createFromFormat($dateFormat, $date)->format('Y-m-d H:i:s');
            return $date;
        }
    }

    public static function userColor($userId)
    {
        $color = ["", "bg-primary", "bg-success", "bg-warning", "bg-danger", "bg-info", "bg-indigo", "bg-purple", "bg-teal", "bg-pink", "bg-orange"];
        $key = self::sumId($userId);
        return $color[$key];
    }

    public static function sumId($id)
    {
        if ($id > 9) {
            $id = array_sum(str_split($id));
            return self::sumId($id);
        } else {
            return $id;
        }
    }

    public static function userShortName($userId = NULL)
    {
        $userId = ($userId) ? (Core::decodeId($userId)) : (Auth::id());
        $user = User::find($userId);
        // $name = preg_split("/\s+/", $user->name);
        $shortName = '';
        // $shortName = substr(ucfirst(reset($name)), 0, 1);
        // $shortName .= ((sizeof($name)) > 1) ? (substr(ucfirst(end($name)), 0, 1)) : '';
        return $shortName;
    }

    public static function userFirstName($userId)
    {
        $user = User::find(Core::decodeId($userId));
        $name = preg_split("/\s+/", $user->name);
        $shortName = ucfirst(reset($name));
        return $shortName;
    }

    public function getFilterStatusSelectList()
    {
        // try {
        $parameters = collect();
        $parameters['search'] = Request::input('q');
        $roles = Core::getActiveStatuses($parameters);
        return $roles;
        // } catch (\PDOException $e) {
        //     throw new catchPdoException($e);
        // } catch (\Exception $e) {
        //     throw new catchException($e);
        // }
    }
}
