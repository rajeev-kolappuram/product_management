<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProfileAccountInformation extends Controller
{
    public function indexAction(Request $request)
    {
        return view('profile::profile_account_information');
    }
}
