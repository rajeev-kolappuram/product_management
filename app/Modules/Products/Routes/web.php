<?php

use App\Modules\Products\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'products'], function () {
    Route::get('/', 'ProductController@index')->name('index');
    Route::post('/list', 'ProductController@getProductsList')->name('get-products-list');
    Route::get('/create', [ProductController::class, 'store'])->name('create');
    Route::post('/select-deatils', 'ProductController@getProductsDetails')->name('get-products-details');
    Route::post('/add-update', 'ProductController@addUpdateProduct')->name('add-update-products');
    Route::get('/select-list', 'ProductController@getProductsSelectList')->name('products-select-list');
    Route::post('/change-status', 'ProductController@changeStatus')->name('change-products-status');
});
