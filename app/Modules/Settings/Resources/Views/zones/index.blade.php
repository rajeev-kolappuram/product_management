@extends('layouts.app',['pageTitle'=>'Zones','menuSettings' => 'kt-menu__item--active kt-menu__item--open','menuSettingsZones' => 'kt-menu__item--active'])

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Zones <small><span class="badge badge-secondary total-records-zones">0</span></small> </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('settings') }}" class="kt-subheader__breadcrumbs-link">Settings</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Zones</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon btn-secondary" onclick="openZoneModal()" title="Add Zone"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Add/Update modal -->
<div class="modal fade" id="zoneModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            {{ Form::open(array('url'=>route('add-update-zone'),'data-redirect-url'=>false,'method'=>'post','id'=>'zoneAddUpdateForm','data-validation'=>'true','data-model'=>'zoneModal','data-data-table'=>'zoneManagementDataTable' ))  }}
            {{ Form::hidden('zone_id','',['id'=>'zoneId'])  }}
            <div class="modal-header">
                <h5 class="modal-title" id="addUpdateModalTitle">Add Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Html::decode(Form::label('zone_name', 'Zone <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('zone_name','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'zoneName',
                        'data-parsley-required-message' =>'Zone is required',
                        'placeholder'                   =>'Enter zone'])  
                    }}
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary mr-2" id="addUpdateBtn" onclick="createOrUpdate(this)">Submit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger mr-2" id="cancelBtn" onclick="cancelForm(this)">Cancel</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- end:: Add/Update modal -->

<!-- begin:: Change status modal -->
<div id="activateDeactivateModal" class="modal fade">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-body text-center pd-y-20 pd-x-20">
                <i id="activateUpdateIcon"></i>
                <h4 id="activateUpdateAreYouSure">Are You Sure?</h4>
                <p class="mg-b-20 mg-x-20" id="activateUpdateText"></p>
                <input type="hidden" value="" id="activateUpdateId">
                <a href="javascript:void(0)" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close" onclick="activateDeactivate()">Yes</a>
                <a href="javascript:void(0)" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close">No</a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Change status modal -->

<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="zoneManagementDataTable" class="table table-bordered" style="width:100%">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center">Sl#</th>
                                <th>Zone</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Portlet-->
@endsection

@section('script')
<script>
    var URL_ZONE_LIST = "{{ route('get-zone-list') }}";
    var URL_ZONE_DETAILS = "{{ route('get-zone-details') }}";
    var URL_CHANGE_STATUS = "{{ route('change-zone-status') }}";
</script>
<script src="{{ asset(mix('js/settings-zones.js')) }}"></script>
@endsection