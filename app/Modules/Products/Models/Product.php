<?php

namespace App\Modules\Products\Models;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

        protected $guarded = [];

        protected $table = 'products';
        public $timestamps = true;
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        // protected $fillable = [
        //     'product_title', 'product_description','colour', 'product_size','product_price','product_stock','img','status',
        // ];
        public function getEnIdAttribute($value)
        {
            return Core::encodeId($value);
        }
        public function getNameAttribute($value)
        {
            return ucfirst(str_replace("_", " ", $value));
        }


        public static function getProductList($limit, $offset, $search, $colName, $sort, $filterData)
        {
            $limit = (int) $limit;
            $offset = (int) $offset;
            $search = $search['value'];
            $sort = ($sort == 'asc')   ? ('asc') : ('desc');
    
            $products = Product::select('id as en_id', 'title','status','description', 'color', 'size', 'stock', 'weight', 'price')
                ->where(function ($query) use ($search) {
                    $query->where('products.title', 'like', $search . '%');
                })
                ->orderBy($colName, $sort);
    
            if (!empty($products)) {
    
                $result = [];
                $result['count'] = $products->count();
                $result['data'] = $products->skip($offset)->take($limit)->get()->toArray();
                return $result;
            }
        }
            public static function getProductsDetails($productsId = NULL)
            {
                if ($productsId) {
                    $productsDetails = Product::select('id as en_id', 'title', 'description', 'color', 'size', 'stock', 'weight', 'price')
                        ->where('id', $productsId)
                        ->first();
                    return ($productsDetails) ? ($productsDetails->toArray()) : [];
                }
                return [];
            }





            public static function getProductsSelecList($filter)
            {
                $search = $filter['search'];
                $data = Product::select('id as en_id', 'title')
                    ->where(function ($query) use ($search) {
                        if ($search) {
                            $query->where('title', 'like', '%' . $search . '%');
                        }
                    })
                    ->where('status', 'active')
                    ->orderBy('name', 'asc')
                    ->get()->toArray();

                $totalCount = Product::where('status', 'active')->count();
                $data = array(
                    "total_count" => $totalCount,
                    "incomplete_results" => true,
                    "items" => $data
                );
                return response()->json($data);
            }
        }

    
