<?php

namespace App\Modules\UserManagement\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\UserManagement\Http\Requests\AddUpdatePermission;
use App\Modules\UserManagement\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Permissions extends Controller
{
    public function index()
    {
        return view('user-management::permissions.index');
    }

    public function getPermissionSelectList()
    {
        try {
            $parameters = collect();
            $parameters['search'] = Request::input('q');
            $parameters['isParentPermission'] = (Request::input('parent_permission')) ? (Request::input('parent_permission')) : false;
            $permissions = Permission::getPermissionSelecList($parameters);
            return $permissions;
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function addUpdatePermission(AddUpdatePermission $request)
    {
        try {
            $permissionId = (Request::input('permission_id')) ? (Core::decodeId(Request::input('permission_id'))) : NULL;
            $permission = Request::input('permission');
            $permissionData = [
                'name'        => Request::input('permission_name'),
                'parent'      => ((Request::input('permission_parent')) && (Request::input('permission_parent') !== 0)) ? (Core::decodeId(Request::input('permission_parent'))) : 0,
                'created_by'  => Auth::id()
            ];
            if ($permissionId) {
                $isPermissionUpdated = Permission::where('id', $permissionId)->update($permissionData);
                if ($isPermissionUpdated) {
                    return response()->json(['status' => 1, 'heading' => 'Permission Updated', 'msg' =>  $permission  . ' updated succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $permission  . ' was not updated succesfully.']);
            } else {
                $isPermissionCreated = Permission::create($permissionData);
                if ($isPermissionCreated) {
                    return response()->json(['status' => 1, 'heading' => 'Permission Created', 'msg' =>  $permission  . ' created succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $permission  . ' was not created succesfully.']);
            }
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getPermissionList()
    {
        try {
            $limit = (Request::input('length') != '') ? Request::input('length') : 10;
            $offset = (Request::input('start') != '') ? Request::input('start') : 0;
            $search = Request::input('search');
            $order = Request::input('order');
            $columns = Request::input('columns');
            $colName = 'permissions.id';
            $sort = 'desc';
            if (isset($order[0]['column']) && isset($order[0]['dir'])) {
                $colNo = $order[0]['column'];
                $sort = $order[0]['dir'];
                if (isset($columns[$colNo]['name'])) {
                    $colName = $columns[$colNo]['name'];
                }
            }
            $filterData = [];
            $result = Permission::getPermissionsList($limit, $offset, $search, $colName, $sort, $filterData);
            $data = ["iTotalDisplayRecords" => $result['count'], "iTotalRecords" => $limit, "TotalDisplayRecords" => $limit];
            $data['data'] = $result['data'];
            return response()->json($data);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getPermissionDetails()
    {
        try {
            $permissionId = (Request::input('permission_id')) ? (Core::decodeId(Request::input('permission_id'))) : NULL;
            if ($permissionId) {
                $permissionDetails = Permission::getPermissionDetails($permissionId);
                if ($permissionDetails) {
                    return response()->json(['status' => 1, 'heading' => 'Permission Details', 'msg' => 'Permission details found successfully', 'data' => $permissionDetails]);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Permission details not found!.', 'data' => []]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Permission details not found!.', 'data' => []]);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function changeStatus()
    {
        try {
            $permissionId = (Request::input('permission_id')) ? (Core::decodeId(Request::input('permission_id'))) : NULL;
            if ($permissionId != '') {
                $permissionDetails = Permission::select('id', 'status')->where('id', $permissionId)->first();
                if ($permissionDetails) {
                    $status = ($permissionDetails['status'] == 'active') ? 'inactive' : 'active';
                    $updateStatus = Permission::where('id', $permissionId)->update(['status' => $status]);
                    if ($updateStatus)
                        return response()->json(['status' => 1, 'heading' => 'Status Updated', 'msg' => 'Permission status updated']);
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Permission status not updated!']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Permission details not found!']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Permission details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getPermissionHierarchyView()
    {
        $permissions = Permission::select('name as permission', 'id', 'parent')->orderBy('parent', 'asc')->get()->toArray();
        $permissionTree = Permissions::arrayToTreeRecursive($permissions, [], '0');
        $permissionTree =  json_encode($permissionTree);
        $html = view('user-management::permissions.permission_hierarchy')->with(['permission' => $permissionTree])->render();
        return response()->json(['status' => 1, 'heading' => 'Something Went Wrong', 'msg' => '', 'html' => $html, 'data' => $permissionTree]);
    }

    public static function arrayToTreeRecursive(array $elements, $rolePermissions, $parentId = '0')
    {
        $return = [];
        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $data = [];
                $data['text'] = $element['permission'];
                $data['id'] = $element['id'];
                $data['state'] = ["selected" => ((in_array($element['id'], $rolePermissions)) ? (true) : (false))];
                $children = self::arrayToTreeRecursive($elements, $rolePermissions, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                    $data['children'] = $children;
                }
                $return[] = $data;
            }
        }
        return $return;
    }
}
