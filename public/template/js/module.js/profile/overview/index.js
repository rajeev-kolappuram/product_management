
$(document).ready(function () {
    loadActiveLogins(); //----------LOAD USER ACTIVE LOGINS
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
function loadActiveLogins() {
    $.ajax({
        type: 'POST',
        data: { _token: _token },
        url: URL_LOAD_ACTIVE_LOGINS,
        success: function (data) {
            if (data.status == 1) {
                $('.active-logins').html(data.html);
            } else {
                toast(data.heading, data.msg, 'warning')
            }
        }
    });
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
function signOutAllConfirm() {
    const { value: password } = Swal.fire({
        title: 'Enter your password',
        input: 'password',
        inputPlaceholder: 'Enter your password',
        inputAttributes: {
            maxlength: 9,
            autocapitalize: 'off',
            autocorrect: 'off'
        },
        confirmButtonText: 'Submit',
        cancelButtonText: 'Cancel',
        showCancelButton: true,
        inputPlaceholder: 'Password',
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value == '') {
                    resolve('Please enter your password.')
                }
                resolve();
            })
        },
        showLoaderOnConfirm: true,
        preConfirm: (password) => {
            return fetch(`${URL_PROCEED_SIGNOUT}`, {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ 'password': password, '_token': _token }),
            }).then((response) => response.json())
                .then((data) => {
                    if (!data.status) {
                        throw new Error(data.msg)
                    }
                    return data;
                })
                .catch((error) => {
                    Swal.showValidationMessage(`${error.message}`)
                });
        },
        allowOutsideClick: false
    }).then((result) => {
        if (result.value.status) {
            Swal.fire(result.value.heading, result.value.msg, 'success');
            loadActiveLogins(); //----------LOAD USER ACTIVE LOGINS

        }
    })
}