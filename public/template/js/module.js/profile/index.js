
$(document).ready(function () {
    getProfileDetails(); //----------GET USER PROFILE DETAILS
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
function getProfileDetails() {
    $.ajax({
        type: 'POST',
        data: { _token: _token },
        url: URL_PROFILE_DETAILS,
        success: function (data) {
            if (data.status == 1) {
                var userDetails = data.data;
                $('.profile-name').html(userDetails.profile_name);
                $('.profile-email').html(userDetails.email);
            } else {
                toast(data.heading, data.msg, 'warning')
            }
        }
    });
}