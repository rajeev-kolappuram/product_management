@extends('layouts.app',['pageTitle'=>'Role Hierarchy','menuUserManagement' => 'kt-menu__item--active kt-menu__item--open','menuUserManagementRolesHierarchy' => 'kt-menu__item--active'])

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Roles <small class="d-none"><span class="badge badge-secondary total-records-roles">0</span></small> </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('user-management') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Roles Hierarchy</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon btn-secondary" onclick="openRoleModal()" title="Add Role"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="alert alert-light alert-elevate" role="alert">
        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
        <div class="alert-text">
            Drag the roles to change hierarchy level.
        </div>
    </div>

    <div class="kt-portlet">
        <div class="kt-portlet__body">
            @if(!empty($roles))
            <div class="dd" id="nestable">
                {{dd('a')}}
                {{ Core::hirarchyHtml($roles); }}
            </div>
            @else
            No roles found.
            @endif
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var URL_updateSortOrder = "{{ URL::to('/user-management/role-hierarchy/updateSortOrder') }}";
</script>
<script type="text/javascript" src="{{ asset('public/modulejs/user-management/assign-role/role_hierarchy.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/modulejs/user-management/assign-role/jquery.nestable.js')}}"></script>
@endsection