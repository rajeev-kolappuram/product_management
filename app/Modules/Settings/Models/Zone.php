<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $guarded = [];

    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getEnZoneIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getNameAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    public static function getZonesList($limit, $offset, $search, $colName, $sort, $filterData)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $search = $search['value'];
        $sort = ($sort == 'asc') ? ('asc') : ('desc');

        $zones = Zone::select('id as en_id', 'name', 'status')
            ->where(function ($query) use ($search) {
                $query->where('zones.name', 'like', $search . '%');
            })
            ->orderBy($colName, $sort);

        if (!empty($zones)) {
            $result = [];
            $result['count'] = $zones->count();
            $result['data'] = $zones->skip($offset)->take($limit)->get()->toArray();
            return $result;
        }
    }

    public static function getZoneDetails($zoneId = NULL)
    {
        if ($zoneId) {
            $zoneDetails = Zone::select('id as en_id', 'name')
                ->where('id', $zoneId)
                ->first();
            return ($zoneDetails) ? ($zoneDetails->toArray()) : [];
        }
        return [];
    }

    public static function getZoneSelecList($filter)
    {
        $search = $filter['search'];
        $data = Zone::select('id as en_id', 'name')
            ->where(function ($query) use ($search) {
                if ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                }
            })
            ->where('status', 'active')
            ->orderBy('name', 'asc')
            ->get()->toArray();

        $totalCount = Zone::where('status', 'active')->count();
        $data = array(
            "total_count" => $totalCount,
            "incomplete_results" => true,
            "items" => $data
        );
        return response()->json($data);
    }
}
