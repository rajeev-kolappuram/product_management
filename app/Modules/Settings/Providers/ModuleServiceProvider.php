<?php

namespace App\Modules\Settings\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('settings', 'Resources/Lang', 'app'), 'settings');
        $this->loadViewsFrom(module_path('settings', 'Resources/Views', 'app'), 'settings');
        $this->loadMigrationsFrom(module_path('settings', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('settings', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('settings', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
