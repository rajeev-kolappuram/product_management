<?php

namespace App\Modules\UserManagement\Models;

use App\Modules\Core\Http\Controllers\Core;
use App\Modules\UserManagement\Http\Controllers\Permissions;
use Illuminate\Database\Eloquent\Model;

class Role extends \Spatie\Permission\Models\Role
{

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public function getRoleAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public static function getRolesList($limit, $offset, $search, $colName, $sort, $filterData)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $search = $search['value'];
        $sort = ($sort == 'asc') ? ('asc') : ('desc');

        $roles = Role::select('id as en_id', 'name as role', 'status')
            ->withCount('role_users')
            ->where(function ($query) use ($search) {
                $query->where('roles.name', 'like', $search . '%');
            })
            ->orderBy($colName, $sort);

        if (!empty($roles)) {

            $result = [];
            $result['count'] = $roles->count();
            $result['data'] = $roles->skip($offset)->take($limit)->get()->toArray();
            return $result;
        }
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public static function getRoleDetails($roleId = NULL)
    {
        if ($roleId) {
            $roleDetails = Role::select('id as en_id', 'name  as role', 'name as role_slug')
                ->where('id', $roleId)
                ->first();
            return ($roleDetails) ? ($roleDetails->toArray()) : [];
        }
        return [];
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public static function getRoleSelecList($filter)
    {
        $search = $filter['search'];
        $data = Role::select('id as en_id', 'name  as role')
            ->where(function ($query) use ($search) {
                if ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                }
            })
            ->where('status', 'active')
            ->orderBy('name', 'asc')
            ->get()->toArray();

        $totalCount = Role::where('status', 'active')->count();
        $data = array(
            "total_count" => $totalCount,
            "incomplete_results" => true,
            "items" => $data
        );
        return response()->json($data);
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public static function getRoles()
    {
        $data = Role::select('id as en_id', 'name as role', 'order')
            ->where('status', 'active')
            ->orderBy('order', 'asc')
            ->get()->toArray();
        return  $data;
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public function role_users()
    {
        return $this->hasMany('App\Modules\UserManagement\Models\ModelHasRole', 'role_id', 'id');
    }
}
