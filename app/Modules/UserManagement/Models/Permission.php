<?php

namespace App\Modules\UserManagement\Models;

use App\Modules\Core\Http\Controllers\Core;

class Permission extends \Spatie\Permission\Models\Permission
{
    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getPermissionAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    public function getParentPermissionAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    public function getEnParentPermissionIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public static function getPermissionSelecList($filter)
    {
        $search = $filter['search'];
        $data = Permission::select('id as en_id', 'name as permission')
            ->where(function ($query) use ($search) {
                if ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                }
            })
            ->where('status', 'active')
            ->orderBy('name', 'asc')
            ->get()->toArray();

        $totalCount = Permission::where('status', 'active')->count();
        if ($filter['isParentPermission']) {
            $parentData = [
                'en_id' => '0',
                'name'  => 'Parent Permission'
            ];
            array_push($data, $parentData);
            $totalCount += 1;
        }

        $data = array(
            "total_count" => $totalCount,
            "incomplete_results" => true,
            "items" => $data
        );
        return response()->json($data);
    }

    public static function getPermissionsList($limit, $offset, $search, $colName, $sort, $filterData)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $search = $search['value'];
        $sort = ($sort == 'asc') ? ('asc') : ('desc');

        $permissions = Permission::select('permissions.id as en_id', 'permissions.name as permission',  'permissions.status', 'parent_permission.name as parent_permission')
            ->leftJoin('permissions as parent_permission', 'permissions.parent', '=', 'parent_permission.id')
            ->where(function ($query) use ($search) {
                $query->where('permissions.name', 'like', $search . '%');
            })
            ->orderBy($colName, $sort);

        if (!empty($permissions)) {
            $result = [];
            $result['count'] = $permissions->count();
            $result['data'] = $permissions->skip($offset)->take($limit)->get()->toArray();
            return $result;
        }
    }

    public static function getPermissionDetails($permissionId = NULL)
    {
        if ($permissionId) {
            $permissionDetails = Permission::select('permissions.id as en_id', 'permissions.name  as permission', 'permissions.status', 'parent_permission.name as parent_permission', 'parent_permission.id as en_parent_permission_id')
                ->leftJoin('permissions as parent_permission', 'permissions.parent', '=', 'parent_permission.id')
                ->where('permissions.id', $permissionId)
                ->first();
            return ($permissionDetails) ? ($permissionDetails->toArray()) : [];
        }
        return [];
    }
}
