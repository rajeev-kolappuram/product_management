
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
function updatePassword(e) {
    var form = $(e).closest("form");
    var validation = form.data('validation');
    var formUrl = form.attr('action');
    var actionButton = $(e);
    var formData = form.serialize();
    if (validation != false) {
        form.parsley({ excluded: ':hidden' }).validate();
        if (!form.parsley().isValid()) {
            return false;
        }
    }
    if (loader != undefined) {
        loader.show();
    }
    actionButton.attr('disabled', 'true')
    $.ajax({
        url: formUrl,
        type: "POST",
        data: formData,
        success: function (data) {
            if (loader != undefined) {
                loader.hide();
            }
            if (data.status == 'validation-error') {
                generateValidationWarning(form, data.errors);
                actionButton.removeAttr('disabled');
            } else if (data.status == 1) {
                loginAgain(data.heading, data.msg);
                actionButton.removeAttr('disabled');
            } else {
                toast(data.heading, data.msg, 'warning');
                actionButton.removeAttr('disabled');
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            ajaxErrorAlert(jqXHR, textStatus, errorThrown);
            if (loader != undefined) {
                loader.hide();
            }
            actionButton.removeAttr('disabled')
        }
    });
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
function loginAgain(heading, message) {
    let timerInterval;
    Swal.fire({
        title: heading,
        html: message,
        timer: delay,
        timerProgressBar: true,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                Swal.getContent().querySelector('b').textContent = Swal.getTimerLeft()
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
            window.location.reload();
        }
    })
}