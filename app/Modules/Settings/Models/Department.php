<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];

    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getNameAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    public static function getDepartmentsList($limit, $offset, $search, $colName, $sort, $filterData)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $search = $search['value'];
        $sort = ($sort == 'asc') ? ('asc') : ('desc');

        $departments = Department::select('id as en_id', 'name', 'status')
            ->where(function ($query) use ($search) {
                $query->where('departments.name', 'like', $search . '%');
            })
            ->orderBy($colName, $sort);

        if (!empty($departments)) {
            $result = [];
            $result['count'] = $departments->count();
            $result['data'] = $departments->skip($offset)->take($limit)->get()->toArray();
            return $result;
        }
    }

    public static function getDepartmentDetails($departmentId = NULL)
    {
        if ($departmentId) {
            $departmentDetails = Department::select('id as en_id', 'name')
                ->where('id', $departmentId)
                ->first();
            return ($departmentDetails) ? ($departmentDetails->toArray()) : [];
        }
        return [];
    }

    public static function getDepartmentSelecList($filter)
    {
        $search = $filter['search'];
        $data = Department::select('id as en_id', 'name')
            ->where(function ($query) use ($search) {
                if ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                }
            })
            ->where('status', 'active')
            ->orderBy('name', 'asc')
            ->get()->toArray();

        $totalCount = Department::where('status', 'active')->count();
        $data = array(
            "total_count" => $totalCount,
            "incomplete_results" => true,
            "items" => $data
        );
        return response()->json($data);
    }
}
