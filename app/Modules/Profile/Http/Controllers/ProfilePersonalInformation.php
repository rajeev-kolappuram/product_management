<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProfilePersonalInformation extends Controller
{
    public function indexAction(Request $request)
    {
        return view('profile::profile_personal_information');
    }
}
