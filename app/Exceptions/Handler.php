<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use Exception;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof catchPdoException) {
            return response()->json([
                'status' => 'pdo-exception',
                'errors' => strtok($exception->getMessage(), "\n")
            ])->setStatusCode(500);
        } else if ($exception instanceof catchException) {
            return response()->json([
                'status' => 'catch-exception',
                'errors' =>  strtok($exception->getMessage(), "\n")
            ])->setStatusCode(500);
        } else  if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return response()->json([
                'status' => 'csrf-exception',
                'errors' =>  strtok($exception->getMessage(), "\n")
            ])->setStatusCode(500);
        } else if ($exception instanceof \Facade\Ignition\Exceptions\ViewException) {
            // return redirect('login');
        }
        return parent::render($request, $exception);
    }

    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json([
            'status' => 'validation-error',
            'errors' => $exception->errors()
        ], $exception->status)->setStatusCode(200);
    }
}
