<?php


namespace App\Modules\Products\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Modules\Products\Models\Product;
use Illuminate\Support\Facades\Request;
use App\Modules\Products\Http\Requests\AddUpdateProduct;
use App\Modules\Core\Http\Controllers\Core;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Flysystem\File;

class ProductController extends Controller
{

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function index()
    {
        return view("products::index");
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function getProductsList()
    {
        try {
            $limit = (Request::input('length') != '') ? Request::input('length') : 10;
            $offset = (Request::input('start') != '') ? Request::input('start') : 0;
            $search = Request::input('search');
            $order = Request::input('order');
            $columns = Request::input('columns');
            $colName = 'id';
            $sort = 'desc';
            if (isset($order[0]['column']) && isset($order[0]['dir'])) {
                $colNo = $order[0]['column'];
                $sort = $order[0]['dir'];
                if (isset($columns[$colNo]['name'])) {
                    $colName = $columns[$colNo]['name'];
                }
            }
            $filterData = [];
            $result = Product::getProductList($limit, $offset, $search, $colName, $sort, $filterData);
            $data = ["iTotalDisplayRecords" => $result['count'], "iTotalRecords" => $limit, "TotalDisplayRecords" => $limit];
            $data['data'] = $result['data'];
            return response()->json($data);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function getProductsDetails()
    {
        try {
            $productsId = (Request::input('products_id')) ? (Core::decodeId(Request::input('products_id'))) : NULL;
            if ($productsId) {
                $productsDetails = Product::getProductsDetails($productsId);
                if ($productsDetails) {
                    return response()->json(['status' => 1, 'heading' => 'products Details', 'msg' => 'products details found successfully', 'data' => $productsDetails]);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'products details not found!.', 'data' => []]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'products details not found!.', 'data' => []]);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function getProductsSelectList()
    {
        try {
            $parameters = collect();
            $parameters['search'] = Request::input('q');
            $products = Product::getProductsSelecList($parameters);
            return $products;
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function changeStatus()
    {
        try {
            $productsId = (Request::input('products_id')) ? (Core::decodeId(Request::input('products_id'))) : NULL;
            if ($productsId) {
                $productsDetails = Product::select('id', 'status')->where('id', $productsId)->first();
                if ($productsDetails) {
                    $status = ($productsDetails['status'] == 'active') ? 'inactive' : 'active';
                    $updateStatus = Product::where('id', $productsId)->update(['status' => $status]);
                    if ($updateStatus)
                        return response()->json(['status' => 1, 'heading' => 'Status Updated', 'msg' => 'Product status updated']);
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Product status not updated!']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Product details not found!']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Product details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function addUpdateProduct(AddUpdateProduct $request)
    {

        $productsId = (Request::input('products_id')) ? (Core::decodeId(Request::input('products_id'))) : NULL;
        $products = Request::input('products');
        $productsData = [
            'title'        => Request::input('products_title'),
            'description'   => Request::input('description'),
            'size' => Request::input('size'),
            'color' => Request::input('color'),
            'weight' => $request->input('weight'),
            'price' => Request::input('price'),
            'stock' => Request::input('stock'),
            'category'  =>  Request::input('category'),
        ];


        if ($request->image) {
            if($productsId) {
                $image_path = Product::where('id', $productsId)->value('image');
                unlink($image_path);
            }

            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            /* Store $imageName name in DATABASE from HERE */
            $path = 'public/images/' . $imageName;
            $productsData['image'] = $path;
        }

        if ($productsId) {
            $isProductsUpdated = Product::where('id', $productsId)->update($productsData);
            if ($isProductsUpdated) {
                return response()->json(['status' => 1, 'heading' => 'Product Updated', 'msg' => $products  . ' updated succesfully.']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $products  . ' was not updated succesfully.']);
        } else {
            $isProductsCreated = Product::create($productsData);

            if ($isProductsCreated) {
                return response()->json(['status' => 1, 'heading' => 'Product Created', 'msg' =>  $products  . ' created succesfully.']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $products  . ' was not created succesfully.']);
        }
    }
}
