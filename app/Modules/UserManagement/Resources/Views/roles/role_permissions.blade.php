<div class="kt-portlet__body">
    <div class="row">
        <div class="col-md-12">
            <div id="assignPermission" class="tree-demo">
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet__foot">
    <div class="kt-form__actions">
        <a href="javascript:confirmRolePermission()" class="btn btn-sm btn-square btn-primary mr-2">Assign Permissions</a>
        <a href="{{ route('user-management-roles') }}" class="btn btn-sm btn-square btn-danger mr-2">Cancel</a>
    </div>
</div>