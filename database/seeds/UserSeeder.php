<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Rajeev R',
            'first_name' => 'Rajeev',
            'last_name' => 'R',
            'employee_id' => 'DEV00001',
            'email' => 'rajeevkolappuram@gmail.com',
            'mobile_number' => '+917736306841',
            'mobile_number_data' => '{
                "iso2": "in",
                "name": "India",
                "number": "7736306841",
                "dialCode": "91"
            }',
            'role_id' => 1,
            'department_id' => 1,
            'password' => Hash::make('iamrajeev'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
