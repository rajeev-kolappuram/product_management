/*------------ CHANGE PASSWORD STARTS HERE ------------*/
if (ability.reset_password) {
    changePassword = (id, name) => {
        $("#passwordResetIcon").addClass('fa fa-paper-plane status-modal-success-icon');
        $("#passwordResetAreYouSure").addClass('');
        $("#passwordResetText").html('Do you want to send password reset link for ' + name);
        $("#passwordResetId").val(id);
        $("#passwordResetWarningModal").modal('show');
    }

    sendPasswordResetLink = () => {
        var userId = $("#passwordResetId").val();
        $.ajax({
            type: 'POST',
            data: { _token: _token, user_id: userId },
            url: URL_PASSWORD_RESET,
            success: function (data) {
                if (data.status == 1) {
                    sweetalert(data.heading, data.msg, 'success')
                    $('#roleManagementDataTable').DataTable().ajax.reload(null, false);
                } else {
                    sweetalert(data.heading, data.msg, 'error')
                }
            }
        });
    }
}
/*------------ CHANGE PASSWORD ENDS HERE ------------*/