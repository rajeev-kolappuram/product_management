<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Profile\Http\Controllers\LoginHistoriesController;
use App\Modules\Profile\Models\LoginHistories;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password', 'status');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $invalidLogins = LoginHistoriesController::getInvalidTryCount($request->email);
        if ($invalidLogins['login_histories_count'] < 3) {
            $remainingLoginAttempts = 2 - $invalidLogins['login_histories_count'];
            $request->request->add(['status' => 'active']);
            if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            $this->incrementLoginAttempts($request);
            if ($invalidLogins['id']) {
                $request->request->add(['user_id' => $invalidLogins['id'], 'is_login' => 'invalid_try']);
                LoginHistoriesController::saveHistory($request);
                throw ValidationException::withMessages([
                    $this->username() => 'These credentials do not match our records. You have ' . ($remainingLoginAttempts) . ' attempts left.'
                ]);
            } else {
                throw ValidationException::withMessages([
                    $this->username() => 'These credentials do not match our records.'
                ]);
            }
        } else {
            throw ValidationException::withMessages([
                $this->username() => 'You exceeded the maximum number of login attempts.Please contact the administator.'
            ]);
        }
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        LoginHistories::where('user_id', ($request->user_id) ? ($request->user_id) : (Auth::id()))
            ->where('status', 'invalid_try')
            ->update(['status' => 'succeeded']);
        LoginHistoriesController::saveHistory($request);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        Artisan::call('cache:clear');
        LoginHistoriesController::updateLogoutHistory($request);
    }
}
