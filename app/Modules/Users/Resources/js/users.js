/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.add_update_user) {
    addUpdateUser = function (userId = '') {
        if (userId != '')
            window.location.href = URL_UPDATE_USER.replace(':userId', userId);
        else
            window.location.href = URL_ADD_USER;
    }
}


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$("#userManagementDataTable").DataTable({
    "pageLength": 25,
    "responsive": true,
    "serverSide": true,
    "ordering": true,
    "aaSorting": [],
    "processing": true,
    "order": [[0, "desc"]],
    "columnDefs": [
        { orderable: false, targets: [0, 8] },
        { "width": "5%", "targets": 0 },
        { "width": "12%", "targets": 1 },
        { "width": "17%", "targets": 2 },
        { "width": "17%", "targets": 3 },
        { "width": "10%", "targets": 4 },
        { "width": "12%", "targets": 5 },
        { "width": "12%", "targets": 6 },
        { "width": "8%", "targets": 7 },
        { "width": "7%", "targets": 8 }

    ],
    "language": {
        "searchPlaceholder": 'Search...',
        "sSearch": '',
        "infoFiltered": " ",
        'loadingRecords': '&nbsp;',
        'processing': dataTableLoader
    },
    "ajax": {
        "url": URL_USER_LIST,
        "type": "post",
        'data': function (data) {
            data._token = _token;
            data.role_id = $("#filterRoleSelect").val();
            data.department_id = $("#filterDepartmentSelect").val();
            data.status = $("#filterStatusSelect").val();
            return data;
        }
    },
    "AutoWidth": false,
    "columns": [
        { "data": "en_id" },
        { "data": "employee_id", "name": "employee_id" },
        { "data": "name", "name": "name" },
        { "data": "email", "name": "email" },
        { "data": "mobile_number", "name": "mobile_number" },
        { "data": "role", "name": "role" },
        { "data": "department", "name": "department" },
        { "data": "status", "name": "status" },
        { "data": "en_id" },
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) {
        loadingShow();
        var info = this.dataTable().api().page.info();
        var page = info.page;
        var length = info.length;
        var index = (page * length + (iDataIndex + 1));

        var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.name) + '","' + aData.status + '")';
        var changePasswordFunction = 'javascript:changePassword("' + aData.en_id + '","' + encodeURIComponent(aData.name) + '")';
        var editFunction = 'javascript:addUpdateUser("' + aData.en_id + '")';
        var viewUserFunction = 'javascript:viewUserDetails("' + aData.en_id + '")';

        if (aData.status == 'active') {
            activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
            activeTitle = 'Inactivate';
            activeIcon = "la la-times-circle";
        } else if (aData.status == 'inactive') {
            activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
            activeTitle = 'Activate';
            activeIcon = "la la-check-circle";
        }

        var htmlData = '';
        htmlData += " <div class=\"dropdown dropdown-inline\">";
        htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        htmlData += "<i class=\"flaticon-more-1\"></i>";
        htmlData += "</button>";
        htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";

        if (ability.add_update_user)
            htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";

        if (ability.change_status_user)
            htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";

        if (ability.reset_password)
            htmlData += "<a class=\"dropdown-item\" href=" + changePasswordFunction + "><i class=\"la la-key\"></i> Reset Password</a>";

        if (ability.view_user)
            htmlData += "<a class=\"dropdown-item\" href=" + viewUserFunction + "><i class=\"la la-eye\"></i> View User Details</a>";

        htmlData += "</div>";
        htmlData += "</div>";

        $('td:eq(0)', nRow).html(index).addClass('text-center');
        $('td:eq(1)', nRow).html(aData.employee_id).addClass('text-center');
        $('td:eq(2)', nRow).html(aData.name).addClass('text-left');
        $('td:eq(3)', nRow).html(aData.email).addClass('text-center');
        $('td:eq(4)', nRow).html(aData.mobile_number).addClass('text-center');
        $('td:eq(5)', nRow).html(aData.role).addClass('text-center');
        $('td:eq(6)', nRow).html(aData.department).addClass('text-center');
        $('td:eq(7)', nRow).html(activeStatus).addClass('text-center');
        $('td:eq(8)', nRow).html(htmlData).addClass('text-center');
    },
    "fnDrawCallback": function (oSettings) {
        var info = this.dataTable().api().page.info();
        var totalRecords = info.recordsDisplay;
        loadingHide();
        updateTotalRecordsCount("total-records-users", totalRecords);
    }

});


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.change_status_user) {
    activateDeactivate = () => {
        var roleId = $("#activateUpdateId").val();
        $.ajax({
            type: 'POST',
            data: { _token: _token, role_id: roleId },
            url: URL_CHANGE_STATUS,
            success: function (data) {
                if (data.status == 1) {
                    sweetalert(data.heading, data.msg, 'success')
                    $('#roleManagementDataTable').DataTable().ajax.reload(null, false);
                } else {
                    sweetalert(data.heading, data.msg, 'error')
                }
            }
        });
    }
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.view_user) {
    viewUserDetails = (userId) => {
        window.location.href = URL_VIEW_USER.replace(':userId', userId);
    }
}

/*------------ FILTER STARTS HERE ------------*/
if (ability.users_filter_section) {
    filterPortalToggle = () => {
        $("#userFilterPortal").slideToggle("slow");
    }


    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    if (ability.users_role_filter) {
        $('#filterRoleSelect').select2({
            placeholder: "Select role",
            minimumResultsForSearch: minimumResultsForSearch,
            ajax: {
                url: URL_ROLE_SELECT_LIST,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                slug: item.name,
                                id: item.en_id
                            }
                        })
                    };
                }
            }
        }).change(function () {
            $(this).parsley().validate();
        });
    }
    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    if (ability.users_department_filter) {
        $('#filterDepartmentSelect').select2({
            placeholder: "Select department",
            minimumResultsForSearch: minimumResultsForSearch,
            ajax: {
                url: URL_DEPARTMENT_SELECT_LIST,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                slug: item.name,
                                id: item.en_id
                            }
                        })
                    };
                }
            }
        }).change(function () {
            $(this).parsley().validate();
        });
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    if (ability.users_status_filter) {
        $('#filterStatusSelect').select2({
            placeholder: "Select status",
            minimumResultsForSearch: minimumResultsForSearch,
            ajax: {
                url: URL_FILTER_STATUS_SELECT_LIST,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item, key) {
                            return {
                                text: item,
                                slug: item,
                                id: key
                            }
                        })
                    };
                }
            }
        }).change(function () {
            $(this).parsley().validate();
        });
    }

    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    applyFilter = () => {
        $('#userManagementDataTable').DataTable().ajax.reload(null, false);
    }


    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    cancelFilter = () => {
        $('#filterRoleSelect').val(null).trigger('change');
        $('#filterDepartmentSelect').val(null).trigger('change');
        $('#filterStatusSelect').val(null).trigger('change');
        $('#userManagementDataTable').DataTable().ajax.reload(null, false);
    }
}
/*------------ FILTER ENDS HERE ------------*/

/*------------ CHANGE PASSWORD STARTS HERE ------------*/
if (ability.reset_password) {
    changePassword = (id, name) => {
        $("#passwordResetIcon").addClass('fa fa-paper-plane status-modal-success-icon');
        $("#passwordResetAreYouSure").addClass('');
        $("#passwordResetText").html('Do you want to send password reset link for ' + name);
        $("#passwordResetId").val(id);
        $("#passwordResetWarningModal").modal('show');
    }

    sendPasswordResetLink = () => {
        var userId = $("#passwordResetId").val();
        $.ajax({
            type: 'POST',
            data: { _token: _token, user_id: userId },
            url: URL_PASSWORD_RESET,
            success: function (data) {
                if (data.status == 1) {
                    sweetalert(data.heading, data.msg, 'success')
                    $('#roleManagementDataTable').DataTable().ajax.reload(null, false);
                } else {
                    sweetalert(data.heading, data.msg, 'error')
                }
            }
        });
    }
}
/*------------ CHANGE PASSWORD ENDS HERE ------------*/