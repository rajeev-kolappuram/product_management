@extends('layouts.auth')

@section('content')
<div class="kt-login__body">
    <div class="kt-login__form">
        <div class="kt-login__title">
            <h3>Sign In</h3>
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input id="email" type="email" placeholder="E-mail Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="kt-login__actions">
                <a class="kt-link kt-login__link-forgot" href="{{ route('register') }}">
                    Register
                </a>
                <button class="btn btn-primary btn-sm btn-elevate kt-login__btn-primary">Sign In</button>
            </div>
        </form>
    </div>
</div>
@endsection