<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>{{ config('app.name', 'Project Setup') }} <?php echo isset($pageTitle) ? (' | ' . $pageTitle) : ''; ?></title>
	<meta name="description" content="Login page example">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<link href="{{ asset('vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('vendors/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(mix('css/auth.css'))}}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="{{ asset('/media/logos/favicon.ico')}}" />
</head>

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
				<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(<?php echo config('app.auth_layout.background') ?>);">
					<div class="kt-grid__item">
						<a href="{{ config('app.url')}}" class="kt-login__logo">
							<img src="{!! config('app.general.logo') !!}">
						</a>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
						<div class="kt-grid__item kt-grid__item--middle">
							<h3 class="kt-login__title">{{ config('app.auth_layout.title')}}</h3>
							<h4 class="kt-login__subtitle">{!! config('app.auth_layout.description')!!}</h4>
						</div>
					</div>
					<div class="kt-grid__item">
						<div class="kt-login__info">
							<div class="kt-login__copyright">
								{!! config('app.general.copy_right') !!}
							</div>
							<div class="kt-login__menu">
								@foreach((array) config('app.general.links') as $linkKey=>$linkUrl)
								<a href="{{ $linkUrl }}" class="kt-link">{{ $linkKey }}</a>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand"   : "#5d78ff",
					"dark"	  : "#282a3c",
					"light"	  : "#ffffff",
					"primary" : "#5867dd",
					"success" : "#34bfa3",
					"info"	  : "#36a3f7",
					"warning" : "#ffb822",
					"danger"  : "#fd3995"
				},
				"base": {
					"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
					"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
				}
			}
		};
	</script>
	<script src="{{ asset('vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
	<script src="{{ asset('vendors/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
	<script src="{{ asset('vendors/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
	<script src="{{ asset('vendors/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('vendors/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset(mix('js/auth.js'))}}" type="text/javascript"></script>
</body>

</html>