"use strict";
// Set defaults
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "slideDown",
    "hideMethod": "slideUp"
}
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
toast = (heading, message, type = 'success') => {
    toastr[type](message, heading, type);
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.loadPlugins = () => {
    KTApp.init(KTAppOptions);
}
