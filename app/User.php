<?php

namespace App;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Billable;
    protected $collection = 'users';
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public function getEnIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getDeIdAttribute($value)
    {
        return Core::decodeId($value);
    }


    public function getEnRoleIdAttribute($value)
    {
        return Core::encodeId($value);
    }


    public function getEnDepartmentIdAttribute($value)
    {
        return Core::encodeId($value);
    }

    public function getRoleAttribute($value)
    {
        return ucfirst(str_replace("_", " ", $value));
    }

    public function login_histories()
    {
        return $this->hasMany('App\Modules\Profile\Models\LoginHistories', 'user_id', 'id');
    }

    public static function getUsersList($limit, $offset, $search, $colName, $sort, $filterData)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $search = $search['value'];
        $sort = ($sort == 'asc') ? ('asc') : ('desc');

        $users = User::select('users.id as en_id', 'users.name', 'users.employee_id', 'users.email', 'users.mobile_number', 'users.status', 'roles.name as role', 'departments.name as department')
            ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
            ->leftJoin('departments', 'users.department_id', '=', 'departments.id')
            ->where(function ($query) use ($search) {
                $query->where('users.name', 'like', $search . '%');
                $query->orWhere('users.employee_id', 'like', $search . '%');
                $query->orWhere('users.email', 'like', $search . '%');
                $query->orWhere('users.mobile_number', 'like', $search . '%');
            })
            ->orderBy($colName, $sort);

        // -----------------FILTER STARTS HERE -----------------------------
        if ($filterData['role_id'])
            $users->where('users.role_id', ($filterData['role_id']));

        if ($filterData['department_id'])
            $users->where('users.department_id', ($filterData['department_id']));

        if ($filterData['status']) {
            if ($filterData['status'] !== 'all')
                $users->where('users.status', ($filterData['status']));
        }
        // -----------------FILTER ENDS HERE -----------------------------

        if (!empty($users)) {
            $result = [];
            $result['count'] = $users->count();
            $result['data'] = $users->skip($offset)->take($limit)->get()->toArray();
            return $result;
        }
    }

    public static function getUserDetails($userId = NULL)
    {
        if ($userId) {
            $userDetails = User::select('users.id as en_id', 'users.name', 'users.first_name', 'users.last_name', 'users.employee_id', 'users.mobile_number_data', 'users.email', 'users.mobile_number', 'users.status', 'roles.id as en_role_id', 'roles.name as role', 'departments.id as en_department_id', 'departments.name as department', 'users.id')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->leftJoin('departments', 'users.department_id', '=', 'departments.id')
                ->with('zones')
                ->where('users.id', $userId)
                ->first();
            return ($userDetails) ? ($userDetails->toArray()) : [];
        }
        return [];
    }

    public function zones()
    {
        return $this->hasMany('App\Modules\Users\Models\UsersZone', 'user_id', 'id')
            ->select('user_id', 'zones.id as en_zone_id', 'zones.name as zone_name')
            ->join('zones', 'zone_id', '=', 'id');
    }
    public function department()
    {
        return $this->belongsTo('App\Modules\Settings\Models\Department');
    }
    
}
