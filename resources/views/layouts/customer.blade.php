<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <title>Home | E-Shopper</title> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('products/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('products/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('products/css/prettyPhoto.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('products/css/price-range.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('products/css/animate.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('products/css/main.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('products/css/responsive.css') }}" type="text/css" rel="stylesheet">
    <!-- [if lt IE 9]> -->
    <!-- <script src="{{ asset('products/js/html5shiv.js') }}"></script>
    <script src="{{ asset('products/js/respond.min.js') }}"></script> -->
    <!-- <![endif]        -->
    <link rel="shortcut icon" href="{{ asset('products/images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('products/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('products/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('products/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('products/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <style>
        #search_field {
            padding: 10px;
            font-size: 17px;
            border: 1px none grey;
            float: right;
            width: 50%;
            background: #f1f1f1;
        }

        #search_button {
            float: right;
            width: 15%;
            padding: 10px;
            background: grey;
            color: white;
            font-size: 17px;
            border: 0px none grey;
            cursor: pointer;
        }
    </style>
    @yield('css')
</head><!--/head-->

<body>
    @include('layouts.customer_header')
    
    @yield('content')
	
    @include('layouts.customer_footer')
    <script src="{{asset('vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/tooltip.js/dist/umd/tooltip.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/wnumb/wNumb.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendors/custom/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('vendors/custom/intl-tel-input/js/intlTelInput-jquery.min.js') }}"></script>
  <script>
            var _token = '{{ csrf_token() }}';
  </script>
    <script src="{{ asset('products/js/jquery.js') }}"></script>
	<script src="{{ asset('products/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('products/js/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('products/js/price-range.js') }}"></script>
    <script src="{{ asset('products/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('products/js/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/parsley.js')}}"></script>
    <script type="text/javascript" src="{{ asset('products/js/customer-app.js')}}"></script>
    @yield('script')
</body>
</html>