<?php

namespace App\Modules\Users\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Settings\Models\Zone;
use App\Modules\UserManagement\Models\Role;
use App\Modules\Users\Http\Requests\AddUpdateUser;
use App\Modules\Users\Models\UsersZone;
use App\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Request;

class Users extends Controller
{
    public static function getUserZones($userId = NULL)
    {
        if ($userId) {
            $zones = UsersZone::select('zones.id as en_zone_id', 'zones.name as zone_name')->join('zones', 'id', '=', 'zone_id')->where('user_id', $userId)->get()->toArray();
        } else {
            $zones = Zone::select('id as en_zone_id', 'name as zone_name')->get()->toArray();
        }
        return $zones;
    }
}
