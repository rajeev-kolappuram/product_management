<?php

namespace App\Modules\UserManagement\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddUpdatePermission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $permissionId = (Request::input('permission_id')) ? (Core::decodeId(Request::input('permission_id'))) : NULL;
        return [
            'permission_name' => ($permissionId) ? ('required|max:255|unique:permissions,name,' . $permissionId . ',id') : ('required|max:255|unique:permissions,name')
        ];
    }

    public function getValidatorInstance()
    {
        $this->manipulateRequest();
        return parent::getValidatorInstance();
    }

    protected function manipulateRequest()
    {
        request()->merge([
            'permission_name' =>  str_slug($this->request->get('permission_name'), '_'),
            'permission' => ucfirst($this->request->get('permission_name'))
        ]);
    }
}
