/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/Modules/UserManagement/Resources/js/assignRolePermission.js":
/*!*************************************************************************!*\
  !*** ./app/Modules/UserManagement/Resources/js/assignRolePermission.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

loadAssignPermission = function loadAssignPermission() {
  var roleId = $("#roleId").val();
  $.ajax({
    type: 'POST',
    data: {
      _token: _token,
      role_id: roleId
    },
    url: URL_ASSIGN_ROLE_PERMISSION,
    success: function success(data) {
      if (data.status == 1) {
        $('#permissionHierarchy').html(data.html);
        var data = JSON.parse(data.data);
        loadJsTree(data);
      } else {
        sweetalert(data.heading, data.msg, 'error');
      }
    }
  });
};

loadAssignPermission();
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */

window.loadJsTree = function (data) {
  $('#assignPermission').jstree({
    'plugins': ["wholerow", "checkbox", "types"],
    'core': {
      "themes": {
        "responsive": true
      },
      'data': data
    },
    "types": {
      "default": {
        "icon": "fa fa-folder kt-font-warning"
      },
      "file": {
        "icon": "fa fa-folder  kt-font-warning"
      }
    }
  }).on('loaded.jstree', function () {
    $('#assignPermission').jstree('open_all');
  });
};
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


assignPermissionsForRole = function assignPermissionsForRole() {
  var selectedRole = $("#roleId").val();
  var selectedPermissions = $('#assignPermission').jstree("get_selected");
  console.log(selectedPermissions);
  $.ajax({
    type: 'POST',
    data: {
      _token: _token,
      role_id: selectedRole,
      permission_ids: selectedPermissions
    },
    url: URL_ASSIGN_PERMISSION_FOR_ROLE,
    success: function success(data) {
      if (data.status == 1) {
        sweetalert(data.heading, data.msg, 'success', true, loadAssignPermission);
      } else {
        sweetalert(data.heading, data.msg, 'warning');
      }
    }
  });
};
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


confirmRolePermission = function confirmRolePermission() {
  $("#confirmationIcon").addClass('fa fa-question-circle status-modal-danger-icon');
  $("#confirmationTitle").html('Are You Sure?');
  $("#confirmationText").html('Do you want to assign the permissions');
  $("#confirmationModal").modal('show');
};

/***/ }),

/***/ 6:
/*!*******************************************************************************!*\
  !*** multi ./app/Modules/UserManagement/Resources/js/assignRolePermission.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ability-shopping/app/Modules/UserManagement/Resources/js/assignRolePermission.js */"./app/Modules/UserManagement/Resources/js/assignRolePermission.js");


/***/ })

/******/ });