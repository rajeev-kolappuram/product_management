<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Profile\Http\Requests\UpdatePasswordRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function indexAction(Request $request)
    {
        return view('profile::profile_change_password');
    }

    public function profileDetails(Request $request)
    {
        try {
            $profileDetails = User::getUserDetails(Auth::id());
            if ($profileDetails) {
                return response()->json(['status' => 1, 'heading' => 'Profile Details', 'msg' => 'User profile details found.', 'data' => $profileDetails]);
            } else {
                return response()->json(['status' => 0, 'heading' => 'Profile Not Found', 'msg' => 'User profile not found.Please refresh and try again.']);
            }
        } catch (\PDOException $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        }
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        try {
            $USER_ID = Auth::id();
            $EN_USER_ID = Core::encodeId($USER_ID);
            $data['current_password'] = Request::input('current_password');
            $data['new_password'] = Request::input('new_password_confirmation');
            $user = User::find($USER_ID);
            if (Hash::check($data['current_password'], $user->password)) {
                $bcryptedPassword = bcrypt($data['new_password']);
                $isPasswordUpdated = User::where(['id' => $USER_ID])->update(['password' => $bcryptedPassword]);
                if ($isPasswordUpdated) {
                    LoginHistoriesController::logoutUserFromAllSessions($EN_USER_ID);
                    return response()->json(['status' => 1, 'heading' => 'Password Changed', 'msg' => 'Your password changed succesfully.<br/>Please login again and keep smiling.<br/><br/>You will be redirected in <b></b> milliseconds.']);
                } else {
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'message' => 'Your password not changed.Please try again.']);
                }
            } else {
                return response()->json([ "status"=>"validation-error","errors"=>["current_password"=>["Current password dosent match.Please enter a valid password."]]]);
            }
        } catch (\PDOException $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $e->getMessage()]);
        }
    }
}
