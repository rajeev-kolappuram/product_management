<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'user-management', 'middleware' => 'auth'], function () {
    Route::get('/', 'UserManagement@index')->name('user-management');

    /* begin: ROLES -------- */
    Route::group(['prefix' => 'roles'], function () {
        Route::post('/add-update',  ['middleware' => ['role_or_permission:admin|add_update_role'], 'uses' => 'Roles@addUpdateRole'])->name('add-update-role');
        Route::post('/list',  ['middleware' => ['role_or_permission:admin|view_role'], 'uses' => 'Roles@getRoleList'])->name('get-role-list');
        Route::post('/details',  ['middleware' => ['role_or_permission:admin|view_role'], 'uses' => 'Roles@getRoleDetails'])->name('get-role-details');
        Route::post('/change-status', ['middleware' => ['role_or_permission:admin|change_status_role'], 'uses' => 'Roles@changeStatus'])->name('change-role-status');
        Route::get('/role-permission/{roleId}',  ['middleware' => ['role_or_permission:admin|assign_role_permissions'], 'uses' => 'Roles@assignRolePermission'])->name('assign-role-permission');
        Route::post('/assign-role-permissions-listing',  ['middleware' => ['role_or_permission:admin|assign_role_permissions'], 'uses' => 'Roles@assignRolePermissionListing'])->name('assign-role-permissions-listing');
        Route::post('/assign-role-permission',  ['middleware' => ['role_or_permission:admin|assign_role_permissions'], 'uses' => 'Roles@assignRolePermissionData'])->name('assign-permission-for-role');
        Route::get('/select-list', 'Roles@getRoleSelectList')->name('role-select-list');
        Route::get('/', ['middleware' => ['role_or_permission:admin|roles_menu'], 'uses' => 'Roles@index'])->name('user-management-roles');

        /* begin: ROLES HIERARCHY -------- */
        Route::group(['prefix' => 'hierarchy'], function () {
            Route::get('/', ['middleware' => ['role_or_permission:admin|role_hierarchy'], 'uses' => 'RolesHierarchy@index'])->name('user-management-roles-hierarchy');
        });
        /* end: ROLES -------- */
    });
    /* end: ROLES -------- */


    /* begin: PERMISSIONS -------- */
    Route::group(['prefix' => 'permissions'], function () {
        Route::post('/add-update', ['middleware' => ['role_or_permission:developer|add_update_permission'], 'uses' => 'Permissions@addUpdatePermission'])->name('add-update-permission');
        Route::post('/list', ['middleware' => ['role_or_permission:developer|view_permission'], 'uses' => 'Permissions@getPermissionList'])->name('get-permission-list');
        Route::post('/details', ['middleware' => ['role_or_permission:developer|view_permission'], 'uses' => 'Permissions@getPermissionDetails'])->name('get-permission-details');
        Route::post('/change-status', ['middleware' => ['role_or_permission:developer|change_status_permission'], 'uses' => 'Permissions@changeStatus'])->name('change-permission-status');
        Route::get('/select-list',  'Permissions@getPermissionSelectList')->name('permissions-select-list');
        Route::post('/hierarchy', ['middleware' => ['role_or_permission:developer|view_permission'], 'uses' => 'Permissions@getPermissionHierarchyView'])->name('permissions-hierarchy-listing');
        Route::get('/', ['middleware' => ['role_or_permission:developer|permissions_menu'], 'uses' => 'Permissions@index'])->name('user-management-permissions');
    });
    /* end: PERMISSIONS -------- */
});
