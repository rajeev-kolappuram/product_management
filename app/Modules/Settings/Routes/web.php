<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'settings', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        dd('This is the Settings module index page. Build something great!');
    })->name('settings');

    /* begin: DEPARTMENTS -------- */
    Route::group(['prefix' => 'departments'], function () {
        Route::post('/add-update', 'Departments@addUpdateDepartment')->name('add-update-department');
        Route::post('/list', 'Departments@getDepartmentList')->name('get-department-list');
        Route::post('/details', 'Departments@getDepartmentDetails')->name('get-department-details');
        Route::post('/change-status', 'Departments@changeStatus')->name('change-department-status');
        Route::get('/select-list', 'Departments@getDepartmentSelectList')->name('department-select-list');
        Route::get('/', 'Departments@index')->name('settings-departments');
    });
    /* end: DEPARTMENTS -------- */


    /* begin: Zones -------- */
    Route::group(['prefix' => 'zone'], function () {
        Route::post('/add-update', 'Zones@addUpdateZone')->name('add-update-zone');
        Route::post('/list', 'Zones@getZoneList')->name('get-zone-list');
        Route::post('/details', 'Zones@getZoneDetails')->name('get-zone-details');
        Route::post('/change-status', 'Zones@changeStatus')->name('change-zone-status');
        Route::get('/select-list', 'Zones@getZoneSelectList')->name('zone-select-list');
        Route::get('/', 'Zones@index')->name('settings-zones');
    });
    /* end: Machine Series -------- */
});
