
updatePassword = (e) => {
    loadingShow();
    var form = $(e).closest("form");
    var validation = form.data('validation');
    var formUrl = form.attr('action');
    var actionButton = $(e);
    var formData = form.serialize();
    if (validation != false) {
        form.parsley({ excluded: ':hidden' }).validate();
        if (!form.parsley().isValid()) {
            loadingHide();
            return false;
        }
    }
    actionButton.attr('disabled', 'true');
    $.ajax({
        url: formUrl,
        type: "POST",
        data: formData,
        success: function (data) {
            if (data.status == 'validation-error') {
                generateValidationWarning(form, data.errors);
                actionButton.removeAttr('disabled');
            } else if (data.status == 1) {
                loginAgain(data.heading, data.msg);
                actionButton.removeAttr('disabled');
            } else {
                toast(data.heading, data.msg, 'warning');
                actionButton.removeAttr('disabled');
            }
            loadingHide();
        }
    });
}

