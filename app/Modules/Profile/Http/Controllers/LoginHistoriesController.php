<?php

namespace App\Modules\Profile\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Profile\Models\LoginHistories;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginHistoriesController extends Controller
{
    public static function saveHistory(Request $request)
    {
        $data = [
            'user_id' => ($request->user_id) ? ($request->user_id) : (Auth::id()),
            'ip_address' => $request->ip(),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'browser_token' => Core::encodeId(time()),
            'status' => ($request->is_login) ? ($request->is_login) : ('logined')
        ];
        Session::put('BROWSER_TOKEN', $data['browser_token']);
        LoginHistories::create($data);
        return true;
    }

    public static function updateLogoutHistory(Request $request)
    {
        $condition = [
            'ip_address' => $request->ip(),
            'user_agent' => $request->server('HTTP_USER_AGENT')
        ];
        $updateData = [
            'status' => 'loggedout'
        ];
        LoginHistories::where($condition)->update($updateData);
        return true;
    }

    public static function logoutUserFromAllSessions($EN_USER_ID)
    {
        $USER_ID = Core::decodeId($EN_USER_ID);
        $updateData = [
            'status' => 'loggedout'
        ];
        LoginHistories::where(['user_id' => $USER_ID])->update($updateData);
        return true;
    }

    public static function getInvalidTryCount($emailId)
    {
        $userDetails = User::select('id')
            ->withCount([
                'login_histories' => function ($sql) {
                    $sql->where('status', 'invalid_try');
                }
            ])
            ->where('email', $emailId)
            ->first();
        if ($userDetails) {
            return [
                'login_histories_count' => ($userDetails->login_histories_count) ? ($userDetails->login_histories_count) : 0,
                'id'                    => ($userDetails->id) ? ($userDetails->id) : NULL
            ];
        }
        return [
            'login_histories_count' =>  0,
            'id'                    => NULL
        ];
    }
}
