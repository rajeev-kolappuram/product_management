<?php

namespace App\Modules\Products\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddUpdateProduct extends FormRequest
{
    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */
    public function rules()
    {
        $productsId = (Request::input('products_id')) ? (Core::decodeId(Request::input('products_id'))) : NULL;
        return [
            'products_title' => ($productsId) ? ('required|max:255|unique:products,title,' . $productsId . ',id') : ('required|max:255|unique:products,title'),
            'description' => ('required|max:255'),
            'size' => ('required|max:255'),
            'color' => ('required|max:255'),
            'stock' => ('required|max:255'),
            'price' => ('required|max:255'),
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
