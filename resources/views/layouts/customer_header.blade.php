<header id="header">
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<h4 style="color:gray;">Ability Shopping</h4>
							{{-- <a href="{{ route('cutomer-index') }}"><img src="{{ asset('products/images/home/logo.png') }}" alt="" /></a> --}}
						</div>
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								<li><a href="{{ route('cutomer-index') }}"><i class="fa fa-crosshairs"></i> Product</a></li>
								<li><a href="{{ route('getProduct') }}"><i class="fa fa-shopping-cart"></i> Buy Product</a></li>
								<li><a href="{{ route('cart') }}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					</div>
					<div class="col-sm-3">
						<form action="{{ route('cutomer-index') }}" method="GET">
							@csrf
							<input type="search" name="search_field" id="search_field" placeholder="Search...">
							<button id="search_button"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
</header><!--/header-->