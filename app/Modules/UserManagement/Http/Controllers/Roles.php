<?php

namespace App\Modules\UserManagement\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\UserManagement\Http\Requests\AddUpdateRole;
use App\Modules\UserManagement\Models\Permission;
use App\Modules\UserManagement\Models\Role;
use App\Modules\UserManagement\Models\RoleHasPermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Roles extends Controller
{
    public function index()
    {
        return view('user-management::roles.index');
    }

    public function addUpdateRole(AddUpdateRole $request)
    {
        try {
            $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
            $role = Request::input('role');
            $roleData = [
                'name'        => Request::input('role_name'),
                'created_by'  => Auth::id()
            ];
            if ($roleId) {
                $isRoleUpdated = Role::where('id', $roleId)->update($roleData);
                if ($isRoleUpdated) {
                    return response()->json(['status' => 1, 'heading' => 'Role Updated', 'msg' => $role  . ' updated succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $role  . ' was not updated succesfully.']);
            } else {
                $isRoleCreated = Role::create($roleData);
                if ($isRoleCreated) {
                    return response()->json(['status' => 1, 'heading' => 'Role Created', 'msg' =>  $role  . ' created succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $role  . ' was not created succesfully.']);
            }
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getRoleList()
    {
        try {
            $limit = (Request::input('length') != '') ? Request::input('length') : 10;
            $offset = (Request::input('start') != '') ? Request::input('start') : 0;
            $search = Request::input('search');
            $order = Request::input('order');
            $columns = Request::input('columns');
            $colName = 'id';
            $sort = 'desc';
            if (isset($order[0]['column']) && isset($order[0]['dir'])) {
                $colNo = $order[0]['column'];
                $sort = $order[0]['dir'];
                if (isset($columns[$colNo]['name'])) {
                    $colName = $columns[$colNo]['name'];
                }
            }
            $filterData = [];
            $result = Role::getRolesList($limit, $offset, $search, $colName, $sort, $filterData);
            $data = ["iTotalDisplayRecords" => $result['count'], "iTotalRecords" => $limit, "TotalDisplayRecords" => $limit];
            $data['data'] = $result['data'];
            return response()->json($data);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getRoleDetails()
    {
        try {
            $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
            if ($roleId) {
                $roleDetails = Role::getRoleDetails($roleId);
                if ($roleDetails) {
                    return response()->json(['status' => 1, 'heading' => 'Role Details', 'msg' => 'Role details found successfully', 'data' => $roleDetails]);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role details not found!.', 'data' => []]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role details not found!.', 'data' => []]);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function changeStatus()
    {
        try {
            $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
            if ($roleId) {
                $roleDetails = Role::select('id', 'status')->where('id', $roleId)->first();
                if ($roleDetails) {
                    $status = ($roleDetails['status'] == 'active') ? 'inactive' : 'active';
                    $updateStatus = Role::where('id', $roleId)->update(['status' => $status]);
                    if ($updateStatus)
                        return response()->json(['status' => 1, 'heading' => 'Status Updated', 'msg' => 'Role status updated']);
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role status not updated!']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role details not found!']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function assignRolePermission($roleId = NULL)
    {
        $roleId = ($roleId) ? (Core::decodeId($roleId)) : NULL;
        $roleDetails = Role::getRoleDetails($roleId);
        return view('user-management::roles.assign_role_permission')->with($roleDetails);
    }

    public function assignRolePermissionListing()
    {
        try {
            $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
            if ($roleId) {
                $rolePermissions = RoleHasPermission::select('permissions.id')
                    ->leftJoin('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                    ->where('role_id', $roleId)
                    ->orderBy('parent', 'asc')
                    ->get()->toArray();
                $rolePermissions = ($rolePermissions) ? (array_column($rolePermissions, 'id')) : [];
                $permissions =  Permission::select('permissions.id as en_id', 'permissions.id', 'permissions.name as permission',  'permissions.status', 'parent_permission.name as parent_permission', 'permissions.parent as parent', 'parent_permission.id as en_parent_permission_id')
                    ->leftJoin('permissions as parent_permission', 'permissions.parent', '=', 'parent_permission.id')
                    ->orderBy('parent', 'asc')
                    ->get()->toArray();
                $permissions = Permissions::arrayToTreeRecursive($permissions, $rolePermissions);
                $permissions =  json_encode($permissions);
                $html = view('user-management::roles.role_permissions')->with(['permission' => $permissions])->render();
                return response()->json(['status' => 1, 'heading' => 'Something Went Wrong', 'msg' => '', 'html' => $html, 'data' => $permissions]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Role details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function assignRolePermissionData()
    {
        try {
            $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
            $permissionIds = (Request::input('permission_ids')) ? (Request::input('permission_ids')) : [];
            $roleDetails = Role::where('id', $roleId)
                ->first();
            $permissions = Permission::whereIn('id', $permissionIds)
                ->get();
            $roleDetails->syncPermissions($permissions);
            return response()->json(['status' => 1, 'heading' => 'Permission assigned', 'msg' => 'Permission assigned for the role successfully.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getRoleSelectList()
    {
        try {
            $parameters = collect();
            $parameters['search'] = Request::input('q');
            $roles = Role::getRoleSelecList($parameters);
            return $roles;
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }
}
