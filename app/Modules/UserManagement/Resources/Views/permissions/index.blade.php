@extends('layouts.app',['pageTitle'=>'Permissions','menuUserManagement' => 'kt-menu__item--active kt-menu__item--open','menuUserManagementPermissions' => 'kt-menu__item--active'])

@section('css')
<link href="{{ asset('vendors/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Permissions <small><span class="badge badge-secondary total-records-permissions">0</span></small> </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('user-management') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Permissions</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @if(auth()->user()->can('add_update_permission') || auth()->user()->hasRole('developer') )
                <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon btn-secondary" onclick="openPermissionModal()" title="Add Permission"><i class="fa fa-plus"></i></button>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->


@if(auth()->user()->can('add_update_permission') || auth()->user()->hasRole('developer') )
<div class="modal fade" id="permissionModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            {{ Form::open(array('url'=>route('add-update-permission'),'data-redirect-url'=>false,'method'=>'post','id'=>'permissionAddUpdateForm','data-validation'=>'true','data-model'=>'permissionModal','data-data-table'=>'permissionManagementDataTable' ))  }}
            {{ Form::hidden('permission_id','',['id'=>'permissionId'])  }}
            <div class="modal-header">
                <h5 class="modal-title" id="addUpdateModalTitle">Add Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Html::decode(Form::label('permission_name', 'Permission <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('permission_name','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'permissionDisplayName',
                        'data-parsley-required-message' =>'Permission is required',
                        'placeholder'                   =>'Enter permission'])  
                    }}
                </div>
                <div class="form-group">
                    {!! Html::decode(Form::label('permission_name', 'Permission <span class="text text-danger d-none"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::select('permission_parent',[],'', [
                        'class'       => 'form-control',
                        'id'          => 'permissionParent',
                        'placeholder' => 'Select parent permission'])
                    }}
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary mr-2" id="addUpdateBtn" onclick="createOrUpdate(this)">Submit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger mr-2" id="cancelBtn" onclick="cancelForm(this)">Cancel</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endif
<!-- end:: Add/Update modal -->

<!-- begin:: Change status modal -->
@if(auth()->user()->can('change_status_permission') || auth()->user()->hasRole('developer') )
<div id="activateDeactivateModal" class="modal fade">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-body text-center pd-y-20 pd-x-20">
                <i id="activateUpdateIcon"></i>
                <h4 id="activateUpdateAreYouSure">Are You Sure?</h4>
                <p class="mg-b-20 mg-x-20" id="activateUpdateText"></p>
                <input type="hidden" value="" id="activateUpdateId">
                <a href="javascript:void(0)" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close" onclick="activateDeactivate()">Yes</a>
                <a href="javascript:void(0)" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close">No</a>
            </div>
        </div>
    </div>
</div>
@endif
<!-- end:: Change status modal -->

<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-sm-8">
            <div class="kt-portlet  kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="permissionManagementDataTable" class="table table-bordered" style="width:100%">
                                <thead class="bg-light">
                                    <tr>
                                        <th class="text-center">Sl#</th>
                                        <th>Permission</th>
                                        <th class="text-center">Parent</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="kt-portlet  kt-portlet--height-fluid" id="permissionHierarchy"></div>
        </div>
    </div>
</div>
<!--end::Portlet-->
@endsection

@section('script')
<script>
    var URL_PERMISSION_LIST = "{{ route('get-permission-list') }}";
    var URL_PERMISSION_DETAILS = "{{ route('get-permission-details') }}";
    var URL_CHANGE_STATUS = "{{ route('change-permission-status') }}";
    var URL_PARENT_PERMISSIONS = "{{ route('permissions-select-list') }}";
    var URL_PERMISSION_HIERARCHY = "{{ route('permissions-hierarchy-listing') }}";
    var ability = {
        'add_update_permission'    : {{ (auth()->user()->can('add_update_permission') || auth()->user()->hasRole('developer'))?1:0  }},
        'view_permission'          : {{ (auth()->user()->can('view_permission') || auth()->user()->hasRole('developer'))?1:0  }},
        'change_status_permission' : {{ (auth()->user()->can('change_status_permission') || auth()->user()->hasRole('developer'))?1:0  }}
    };
</script>
<script src="{{ asset('vendors/custom/jstree/jstree.bundle.min.js') }}"></script>
<script src="{{ asset(mix('js/user-management-permissions.js')) }}"></script>
@endsection