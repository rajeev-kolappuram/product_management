/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/Modules/Settings/Resources/js/zonesManagement.js":
/*!**************************************************************!*\
  !*** ./app/Modules/Settings/Resources/js/zonesManagement.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
openZoneModal = function openZoneModal() {
  var zoneId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  resetForm('#zoneAddUpdateForm');

  if (zoneId != '') {
    $.ajax({
      url: URL_ZONE_DETAILS,
      type: "POST",
      data: {
        _token: _token,
        zone_id: zoneId
      },
      success: function success(result) {
        if (result.status == 1) {
          var data = result.data;
          $("#addUpdateModalTitle").text('Update Zone');
          $("#zoneId").val(data.en_id);
          $("#zoneName").val(data.name);
          $("#zoneModal").modal('show');
        } else {
          toast(result.heading, result.msg, 'warning');
          actionButton.removeAttr('disabled');
        }

        loadingHide();
      }
    });
  } else {
    $("#addUpdateModalTitle").text('Add Zone');
    $("#zoneModal").modal('show');
  }
};
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


$("#zoneManagementDataTable").DataTable({
  "pageLength": 25,
  "responsive": true,
  "serverSide": true,
  "ordering": true,
  "aaSorting": [],
  "processing": true,
  "order": [[0, "desc"]],
  "columnDefs": [{
    orderable: false,
    targets: [0, 3]
  }, {
    "width": "5%",
    "targets": 0
  }, {
    "width": "70%",
    "targets": 1
  }, {
    "width": "18%",
    "targets": 2
  }, {
    "width": "7%",
    "targets": 3
  }],
  "language": {
    "searchPlaceholder": 'Search...',
    "sSearch": '',
    "infoFiltered": " ",
    'loadingRecords': '&nbsp;',
    'processing': dataTableLoader
  },
  "ajax": {
    "url": URL_ZONE_LIST,
    "type": "post",
    'data': function data(_data) {
      _data._token = _token;
      return _data;
    }
  },
  "AutoWidth": false,
  "columns": [{
    "data": "en_id"
  }, {
    "data": "name",
    "name": "name"
  }, {
    "data": "status",
    "name": "status"
  }, {
    "data": "en_id"
  }],
  "fnCreatedRow": function fnCreatedRow(nRow, aData, iDataIndex) {
    loadingShow();
    var info = this.dataTable().api().page.info();
    var page = info.page;
    var length = info.length;
    var index = page * length + (iDataIndex + 1);
    var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.name) + '","' + aData.status + '")';
    var editFunction = 'javascript:openZoneModal("' + aData.en_id + '")';

    if (aData.status == 'active') {
      activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
      activeTitle = 'Inactivate';
      activeIcon = "la la-times-circle";
    } else if (aData.status == 'inactive') {
      activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
      activeTitle = 'Activate';
      activeIcon = "la la-check-circle";
    }

    var htmlData = '';
    htmlData += " <div class=\"dropdown dropdown-inline\">";
    htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
    htmlData += "<i class=\"flaticon-more-1\"></i>";
    htmlData += "</button>";
    htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";
    htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";
    htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";
    htmlData += "</div>";
    htmlData += "</div>";
    $('td:eq(0)', nRow).html(index).addClass('text-center');
    $('td:eq(1)', nRow).html(aData.name).addClass('text-left');
    $('td:eq(2)', nRow).html(activeStatus).addClass('text-center');
    $('td:eq(3)', nRow).html(htmlData).addClass('text-center');
  },
  "fnDrawCallback": function fnDrawCallback(oSettings) {
    var info = this.dataTable().api().page.info();
    var totalRecords = info.recordsDisplay;
    loadingHide();
    updateTotalRecordsCount("total-records-zones", totalRecords);
  }
});
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */

activateDeactivate = function activateDeactivate() {
  var zoneId = $("#activateUpdateId").val();
  $.ajax({
    type: 'POST',
    data: {
      _token: _token,
      zone_id: zoneId
    },
    url: URL_CHANGE_STATUS,
    success: function success(data) {
      if (data.status == 1) {
        sweetalert(data.heading, data.msg, 'success');
        $('#zoneManagementDataTable').DataTable().ajax.reload(null, false);
      } else {
        sweetalert(data.heading, data.msg, 'error');
      }
    }
  });
};

/***/ }),

/***/ 9:
/*!********************************************************************!*\
  !*** multi ./app/Modules/Settings/Resources/js/zonesManagement.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ability-shopping/app/Modules/Settings/Resources/js/zonesManagement.js */"./app/Modules/Settings/Resources/js/zonesManagement.js");


/***/ })

/******/ });