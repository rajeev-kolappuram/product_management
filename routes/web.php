<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', function () {
    return redirect('login');
});
Route::any('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Auth::routes([
    'register' => true, // Registration Routes...
]);

// Auth::routes();
Route::any('/dashboard', 'HomeController@index')->name('dashboard');
Route::any('/products', 'ProductController@index')->name('products');
