/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$('#roleSelect').select2({
    placeholder: "Select role",
    minimumResultsForSearch: minimumResultsForSearch,
    ajax: {
        url: URL_ROLE_SELECT_LIST,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page,
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.items, function (item) {
                    return {
                        text: item.role,
                        slug: item.role,
                        id: item.en_id
                    }
                })
            };
        }
    }
}).change(function () {
    $(this).parsley().validate();
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$('#departmentSelect').select2({
    placeholder: "Select department",
    minimumResultsForSearch: minimumResultsForSearch,
    ajax: {
        url: URL_DEPARTMENT_SELECT_LIST,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page,
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.items, function (item) {
                    return {
                        text: item.name,
                        slug: item.name,
                        id: item.en_id
                    }
                })
            };
        }
    }
}).change(function () {
    $(this).parsley().validate();
});

numberCodeDetailsContact("#mobileNumberTxt", "#mobileNumberDataTxt");
$('#mobileNumberTxt').blur(function () {
    numberCodeDetailsContact("#mobileNumberTxt", "#mobileNumberDataTxt");
});
