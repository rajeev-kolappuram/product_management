/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/Modules/Profile/Resources/js/overview/index.js":
/*!************************************************************!*\
  !*** ./app/Modules/Profile/Resources/js/overview/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {
  loadActiveLogins(); //----------LOAD USER ACTIVE LOGINS
});
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */

loadActiveLogins = function loadActiveLogins() {
  $.ajax({
    type: 'POST',
    data: {
      _token: _token
    },
    url: URL_LOAD_ACTIVE_LOGINS,
    success: function success(data) {
      if (data.status == 1) {
        $('.active-logins').html(data.html);
      } else {
        toast(data.heading, data.msg, 'warning');
      }
    }
  });
};
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


signOutAllConfirm = function signOutAllConfirm() {
  var _Swal$fire;

  var _Swal$fire$then = Swal.fire((_Swal$fire = {
    title: 'Enter your password',
    input: 'password',
    inputPlaceholder: 'Enter your password',
    inputAttributes: {
      maxlength: 9,
      autocapitalize: 'off',
      autocorrect: 'off'
    },
    confirmButtonText: 'Submit',
    cancelButtonText: 'Cancel',
    showCancelButton: true
  }, _defineProperty(_Swal$fire, "inputPlaceholder", 'Password'), _defineProperty(_Swal$fire, "inputValidator", function inputValidator(value) {
    return new Promise(function (resolve) {
      if (value == '') {
        resolve('Please enter your password.');
      }

      resolve();
    });
  }), _defineProperty(_Swal$fire, "showLoaderOnConfirm", true), _defineProperty(_Swal$fire, "preConfirm", function preConfirm(password) {
    return fetch("".concat(URL_PROCEED_SIGNOUT), {
      method: 'POST',
      // or 'PUT'
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'password': password,
        '_token': _token
      })
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      if (!data.status) {
        throw new Error(data.msg);
      }

      return data;
    })["catch"](function (error) {
      Swal.showValidationMessage("".concat(error.message));
    });
  }), _defineProperty(_Swal$fire, "allowOutsideClick", false), _Swal$fire)).then(function (result) {
    if (result.value.status) {
      Swal.fire(result.value.heading, result.value.msg, 'success');
      loadActiveLogins(); //----------LOAD USER ACTIVE LOGINS
    }
  }),
      password = _Swal$fire$then.value;
};

/***/ }),

/***/ 4:
/*!******************************************************************!*\
  !*** multi ./app/Modules/Profile/Resources/js/overview/index.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ability-shopping/app/Modules/Profile/Resources/js/overview/index.js */"./app/Modules/Profile/Resources/js/overview/index.js");


/***/ })

/******/ });