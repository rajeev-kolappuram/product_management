@extends('layouts.app',['pageTitle'=>'Roles','menuUserManagement' => 'kt-menu__item--active kt-menu__item--open','menuUserManagementRoles' => 'kt-menu__item--active'])

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Roles <small><span class="badge badge-secondary total-records-roles">0</span></small> </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('user-management') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Roles</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @if(auth()->user()->can('add_update_role') || auth()->user()->hasRole('admin') )
                <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon btn-secondary" onclick="openRoleModal()" title="Add Role"><i class="fa fa-plus"></i></button>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Add/Update modal -->
@if(auth()->user()->can('add_update_role') || auth()->user()->hasRole('admin') )
<div class="modal fade" id="roleModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            {{ Form::open(array('url'=>route('add-update-role'),'data-redirect-url'=>false,'method'=>'post','id'=>'roleAddUpdateForm','data-validation'=>'true','data-model'=>'roleModal','data-data-table'=>'roleManagementDataTable' ))  }}
            {{ Form::hidden('role_id','',['id'=>'roleId'])  }}
            <div class="modal-header">
                <h5 class="modal-title" id="addUpdateModalTitle">Add Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Html::decode(Form::label('role_name', 'Role <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('role_name','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'roleDisplayName',
                        'data-parsley-required-message' =>'Role is required',
                        'placeholder'                   =>'Enter role'])  
                    }}
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary mr-2" id="addUpdateBtn" onclick="createOrUpdate(this)">Submit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger mr-2" id="cancelBtn" onclick="cancelForm(this)">Cancel</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endif
<!-- end:: Add/Update modal -->

<!-- begin:: Change status modal -->
@if(auth()->user()->can('change_status_role') || auth()->user()->hasRole('admin') )
<div id="activateDeactivateModal" class="modal fade">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-body text-center pd-y-20 pd-x-20">
                <i id="activateUpdateIcon"></i>
                <h4 id="activateUpdateAreYouSure">Are You Sure?</h4>
                <p class="mg-b-20 mg-x-20" id="activateUpdateText"></p>
                <input type="hidden" value="" id="activateUpdateId">
                <a href="javascript:void(0)" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close" onclick="activateDeactivate()">Yes</a>
                <a href="javascript:void(0)" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close">No</a>
            </div>
        </div>
    </div>
</div>
@endif
<!-- end:: Change status modal -->

<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="roleManagementDataTable" class="table table-bordered" style="width:100%">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center">Sl#</th>
                                <th>Role</th>
                                <th class="text-center">Users</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Portlet-->
@endsection

@section('script')
<script>
    var URL_ROLE_LIST = "{{ route('get-role-list') }}";
    var URL_ROLE_DETAILS = "{{ route('get-role-details') }}";
    var URL_CHANGE_STATUS = "{{ route('change-role-status') }}";
    var URL_ASSIGN_ROLE_PERMISSION = "{{ route('assign-role-permission',':roleId') }}";
    var ability = {
        'add_update_role'         : {{ (auth()->user()->can('add_update_role') || auth()->user()->hasRole('admin'))?1:0  }},
        'view_role'               : {{ (auth()->user()->can('view_role') || auth()->user()->hasRole('admin'))?1:0  }},
        'change_status_role'      : {{ (auth()->user()->can('change_status_role') || auth()->user()->hasRole('admin'))?1:0  }},
        'assign_role_permissions' : {{ (auth()->user()->can('assign_role_permissions') || auth()->user()->hasRole('admin'))?1:0  }},
    };
</script>
<script src="{{ asset(mix('js/user-management-roles.js')) }}"></script>
@endsection