
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.add_update_permission) {
    openPermissionModal = function (permissionId = '') {
        resetForm('#permissionAddUpdateForm');
        if (permissionId != '') {
            $.ajax({
                url: URL_PERMISSION_DETAILS,
                type: "POST",
                data: { _token: _token, permission_id: permissionId },
                success: function (result) {
                    if (result.status == 1) {
                        let data = result.data;
                        $("#addUpdateModalTitle").text('Update Permission');
                        $("#permissionId").val(data.en_id);
                        $("#permissionDisplayName").val(data.permission);
                        if (data.en_parent_permission_id)
                            $('#permissionParent').empty().append("<option value='" + data.en_parent_permission_id + "' selected>" + data.parent_permission + "</option>").trigger('change');
                        $("#permissionModal").modal('show');
                    } else {
                        toast(result.heading, result.msg, 'warning');
                        actionButton.removeAttr('disabled');
                    }
                    loadingHide();
                }
            })

        } else {
            $("#addUpdateModalTitle").text('Add Permission');
            $("#permissionModal").modal('show');
        }
    }
}


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$("#permissionManagementDataTable").DataTable({
    "pageLength": 25,
    "responsive": true,
    "serverSide": true,
    "ordering": true,
    "aaSorting": [],
    "processing": true,
    "order": [[0, "desc"]],
    "columnDefs": [
        { orderable: false, targets: [0, 4] },
        { "width": "5%", "targets": 0 },
        { "width": "40%", "targets": 1 },
        { "width": "40%", "targets": 2 },
        { "width": "8%", "targets": 3 },
        { "width": "7%", "targets": 4 }

    ],
    "language": {
        "searchPlaceholder": 'Search...',
        "sSearch": '',
        "infoFiltered": " ",
        'loadingRecords': '&nbsp;',
        'processing': dataTableLoader
    },
    "ajax": {
        "url": URL_PERMISSION_LIST,
        "type": "post",
        'data': function (data) {
            data._token = _token;
            return data;
        }
    },
    "AutoWidth": false,
    "columns": [
        { "data": "en_id" },
        { "data": "permission", "name": "permission" },
        { "data": "parent_permission", "name": "parent_permission" },
        { "data": "status", "name": "status" },
        { "data": "en_id" },
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) {
        loadingShow();
        var info = this.dataTable().api().page.info();
        var page = info.page;
        var length = info.length;
        var index = (page * length + (iDataIndex + 1));

        var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.permission) + '","' + aData.status + '")';
        var editFunction = 'javascript:openPermissionModal("' + aData.en_id + '")';

        if (aData.status == 'active') {
            activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
            activeTitle = 'Inactivate';
            activeIcon = "la la-times-circle";
        } else if (aData.status == 'inactive') {
            activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
            activeTitle = 'Activate';
            activeIcon = "la la-check-circle";
        }

        var htmlData = '';
        htmlData += " <div class=\"dropdown dropdown-inline\">";
        htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        htmlData += "<i class=\"flaticon-more-1\"></i>";
        htmlData += "</button>";
        htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";

        if (ability.add_update_permission)
            htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";

        if (ability.change_status_permission)
            htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";

        htmlData += "</div>";
        htmlData += "</div>";

        $('td:eq(0)', nRow).html(index).addClass('text-center');
        $('td:eq(1)', nRow).html(aData.permission).addClass('text-left');
        $('td:eq(2)', nRow).html(aData.parent_permission).addClass('text-center');
        $('td:eq(3)', nRow).html(activeStatus).addClass('text-center');
        $('td:eq(4)', nRow).html(htmlData).addClass('text-center');
    },
    "fnDrawCallback": function (oSettings) {
        var info = this.dataTable().api().page.info();
        var totalRecords = info.recordsDisplay;
        loadPermissionHierarchy();
        loadingHide();
        updateTotalRecordsCount("total-records-permissions", totalRecords);
    }
});


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.change_status_permission) {
    activateDeactivate = () => {
        var permissionId = $("#activateUpdateId").val();
        $.ajax({
            type: 'POST',
            data: { _token: _token, permission_id: permissionId },
            url: URL_CHANGE_STATUS,
            success: function (data) {
                if (data.status == 1) {
                    sweetalert(data.heading, data.msg, 'success')
                    $('#permissionManagementDataTable').DataTable().ajax.reload(null, false);
                } else {
                    sweetalert(data.heading, data.msg, 'error')
                }
            }
        });
    }
}


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
$('#permissionParent').select2({
    placeholder: "Select Parent Permission",
    minimumResultsForSearch: minimumResultsForSearch,
    ajax: {
        url: URL_PARENT_PERMISSIONS,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page,
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.items, function (item) {
                    return {
                        text: item.permission,
                        slug: item.permission,
                        id: item.en_id
                    }
                })
            };
        }
    }
}).change(function () {
    $(this).parsley().validate();
});

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
loadPermissionHierarchy = () => {
    $.ajax({
        type: 'POST',
        data: { _token: _token },
        url: URL_PERMISSION_HIERARCHY,
        success: function (data) {
            if (data.status == 1) {
                $("#permissionHierarchy").html(data.html);
                var data = JSON.parse(data.data);
                loadJsTree(data);
            }
        }
    })
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.loadJsTree = (data) => {
    $('#permissionHierarchyTree').jstree({
        'plugins': ["wholerow", "types"],
        'core': {
            "themes": {
                "responsive": true
            },
            'data': data
        },
        "types": {
            "default": {
                "icon": "fa fa-folder kt-font-warning"
            },
            "file": {
                "icon": "fa fa-folder  kt-font-warning"
            }
        },
    }).on('loaded.jstree', function () {
        $('#permissionHierarchyTree').jstree('open_all');
    });
}