<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            Permissions
        </h3>
    </div>
</div>
<div class="kt-portlet__body">
    <div class="row">
        <div class="col-md-12">
            <div id="permissionHierarchyTree" class="tree-demo">
            </div>
        </div>
    </div>
</div>