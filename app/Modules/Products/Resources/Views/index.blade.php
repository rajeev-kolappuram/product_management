@extends('layouts.app',['pageTitle'=>'Products','menuSettings' => 'kt-menu_item--active kt-menuitem--open','menuSettingsproducts' => 'kt-menu_item--active'])

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Products <small><span class="badge badge-secondary total-records-orders">0</span></small> </h3>
            <span class="kt-subheader_separator kt-subheader_separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                {{-- <span class="kt-subheader__breadcrumbs-separator"></span> --}}
                {{-- <a href="{{ route('settings') }}" class="kt-subheader__breadcrumbs-link">Settings</a> --}}
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Products</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @hasrole('admin|user')
                    <button type="button" class="btn btn-outline-hover-info btn-elevate btn-icon btn-secondary" onclick="openProductModal()" title="Add Products"><i class="fa fa-plus"></i></button>
                @endhasrole
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->
<!-- begin:: Add/Update modal -->
<div class="modal fade" id="productsModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="products">
        <div class="modal-content">
            {{ Form::open(array('url'=>route('add-update-products'),'data-redirect-url'=>false,'method'=>'post','id'=>'productsAddUpdateForm','data-validation'=>'true','data-model'=>'productsModal','data-data-table'=>'productManagementDataTable' ))  }}
            {{ Form::hidden('products_id','',['id'=>'productsId'])  }}
            <div class="modal-header">
                <h5 class="modal-title" id="addUpdateModalTitle">Add Products</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('products_title', 'Product <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('products_title','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'Products_title',
                        'data-parsley-required-message' =>'Product is required',
                        'placeholder'                   =>'Enter product'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('description', 'Description <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('description','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'description',
                        'data-parsley-required-message' =>'Description is required',
                        'placeholder'                   =>'Enter Description'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('color', 'Color <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::select('color', ['white'=>'white', 'black'=>'black', 'yellow'=>'yellow', 'red'=>'red', 'green'=>'green', 'blue'=>'blue', 'brown'=>'brown'], '',[
                        'class' => 'form-control',
                        'id'=>'colorSelect',
                        'placeholder'=>'select a color'
                    ]) }}
                    <!-- addUpdateUser.js -->
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('category', 'Category <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::select('category', ['Paper'=>'Paper', 'Umbrella'=>'Umbrella', 'Others'=>'Others'], '',[
                        'class' => 'form-control',
                        'id'=>'category',
                        'placeholder'=>'select a category'
                    ]) }}
                    <!-- addUpdateUser.js -->
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('size', 'Size <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('size','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'size',
                        'data-parsley-required-message' =>'Size is required',
                        'placeholder'                   =>'Enter size of product'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('stock', 'Stock <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('stock','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'stock',
                        'data-parsley-required-message' =>'Stock is required',
                        'placeholder'                   =>'Enter Stock of product'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('weight', 'Weight <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('weight','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'weight',
                        'data-parsley-required-message' =>'weight is required',
                        'placeholder'                   =>'Enter weight of product'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('price', 'Price <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::text('price','',[
                        'class'                         =>'form-control',
                        'required'                      =>'required',
                        'maxlength'                     =>'250',
                        'autofocus'                     =>'on',
                        'id'                            =>'price',
                        'data-parsley-required-message' =>'Price is required',
                        'placeholder'                   =>'Enter price of product'])  
                    }} 
                    <!-- Laravel collective ends -->
                </div>

                <div class="form-group">
                    <!-- Laravel collective starts -->
                    {!! Html::decode(Form::label('image', 'Image <span class="text text-danger"> *</span>',['class'=>'form-control-label']))!!}
                    {{ Form::file('image') }} 
                    <!-- Laravel collective ends -->
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary mr-2" id="addUpdateBtn" onclick="createOrUpdateWithFile(this)">Submit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger mr-2" id="cancelBtn" onclick="cancelForm(this)">Cancel</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- end:: Add/Update modal -->

<!-- begin:: Change status modal -->
<div id="activateDeactivateModal" class="modal fade">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-body text-center pd-y-20 pd-x-20">
                <i id="activateUpdateIcon"></i>
                <h4 id="activateUpdateAreYouSure">Are You Sure?</h4>
                <p class="mg-b-20 mg-x-20" id="activateUpdateText"></p>
                <input type="hidden" value="" id="activateUpdateId">
                <a href="javascript:void(0)" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close" onclick="activateDeactivate()">Yes</a>
                <a href="javascript:void(0)" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close">No</a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Change status modal -->



<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid_item kt-grid_item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="productManagementDataTable" class="table table-bordered" style="width:100%">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center">Sl#</th>
                                <th class="text-left">Title</th>
                                <th class="text-left">Description</th>
                                <th class="text-center">Color</th>
                                <th class="text-center">Size</th>
                                <th class="text-center">Stock</th>
                                <th class="text-center">Weight</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!--end::Portlet-->
@endsection


@section('script')
<script>
    var URL_PRODUCT_LIST = "{{ route('get-products-list') }}";
    var URL_PRODUCTS_DETAILS = "{{ route('get-products-details') }}";
    var URL_CHANGE_STATUS = "{{ route('change-products-status') }}";
</script>
<script src="{{ asset(mix('js/products-list.js')) }}"></script>
@endsection