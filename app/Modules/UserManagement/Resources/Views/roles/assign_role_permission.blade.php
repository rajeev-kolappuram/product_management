@extends('layouts.app',['pageTitle'=>'Assign Role Permissions','menuUserManagement' => 'kt-menu__item--active kt-menu__item--open','menuUserManagementRoles' => 'kt-menu__item--active'])

@section('css')
<link href="{{ asset('vendors/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Roles <small><span class="badge badge-secondary total-records-roles">0</span></small> </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('user-management') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('user-management-roles') }}" class="kt-subheader__breadcrumbs-link">Roles</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link">Assign Role Permissions</a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Change status modal -->
<div id="confirmationModal" class="modal fade">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-body text-center pd-y-20 pd-x-20">
                <i id="confirmationIcon"></i>
                <h4 id="confirmationTitle">Are You Sure?</h4>
                <p class="mg-b-20 mg-x-20" id="confirmationText"></p>
                <a href="javascript:void(0)" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close" onclick="assignPermissionsForRole()">Yes</a>
                <a href="javascript:void(0)" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-spacing-2 mg-b-20" data-dismiss="modal" aria-label="Close">No</a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Change status modal -->

<!--begin::Portlet-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        {{ Form::hidden('role_id',(isset($en_id)?($en_id):('')), [
                            'id'=>'roleId'])
                        }}
                        @isset($en_id)
                        {!! Html::decode(Form::label('role_name', 'Role <span class="text text-danger d-none"> *</span>',['class'=>'form-control-label']))!!}
                        {{ Form::text('role_name',($role),[
                            'class'                         =>'form-control',
                            'required'                      =>'required',
                            'maxlength'                     =>'250',
                            'readonly'                      =>'true',
                            'id'                            =>'roleDisplayName',
                            'data-parsley-required-message' =>'Role is required',
                            'placeholder'                   =>'Enter role'])  
                        }}
                        @else
                        {!! Html::decode(Form::label('permission', 'Role <span class="text text-danger d-none"> *</span>',['class'=>'form-control-label']))!!}
                        {{ Form::select('permission',[],'', [
                            'class'       => 'form-control',
                            'id'          => 'permission',
                            'placeholder' => 'Select permission'])
                        }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet" id="permissionHierarchy"></div>
</div>
<!--end::Portlet-->
@endsection

@section('script')
<script>
    var URL_ASSIGN_ROLE_PERMISSION = "{{ route('assign-role-permissions-listing') }}";
    var URL_ASSIGN_PERMISSION_FOR_ROLE = "{{ route('assign-permission-for-role') }}";
    var URL_ASSIGN_ROLE_PERMISSION_VIEW = "{{ route('assign-role-permission',':roleId') }}";
</script>
<script src="{{ asset('vendors/custom/jstree/jstree.bundle.min.js') }}"></script>
<script src="{{ asset(mix('js/user-management-assign-permissions.js')) }}"></script>
@endsection