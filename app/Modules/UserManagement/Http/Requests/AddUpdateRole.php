<?php

namespace App\Modules\UserManagement\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AddUpdateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roleId = (Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : NULL;
        return [
            'role_name' => ($roleId) ? ('required|max:255|unique:roles,name,' . $roleId . ',id') : ('required|max:255|unique:roles,name')
        ];
    }

    public function getValidatorInstance()
    {
        $this->manipulateRequest();
        return parent::getValidatorInstance();
    }

    protected function manipulateRequest()
    {
        request()->merge([
            'role_name' =>  str_slug($this->request->get('role_name'), '_'),
            'role' => ucfirst($this->request->get('role_name'))
        ]);
    }
}
