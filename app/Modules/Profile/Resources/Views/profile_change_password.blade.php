@extends('profile::index',['pageTitle'=>'Change Password','profile_change_password_active' =>'kt-widget__item--active','profile_title'=>'Change Password' ])

@section('profile_content')
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Change Password</h3>
                </div>
            </div>
            {{ Form::open(array('url'=>route('profile-update-password'),'method'=>'post','id'=>'changePasswordForm','data-validation'=>'true','class'=>'kt-form kt-form--label-right' ))  }}
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <div class="kt-section__body">
                        <div class="form-group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Current Password<span class="text text-danger"> *</span></label>
                            <div class="col-lg-8 col-xl-8">
                                <input class="form-control" minlength="6" maxlength="15" placeholder="Enter current password" required="required" name="current_password" type="password" data-parsley-required-message="Current password is required" data-parsley-minlength-message="This current password is too short. It should have 6 characters or more.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">New Password<span class="text text-danger"> *</span></label>
                            <div class="col-lg-8 col-xl-8">
                                <input class="form-control" minlength="6" maxlength="15" placeholder="Enter new password" required="required" name="new_password" type="password" data-parsley-required-message="New password is required" id="newPassword" data-parsley-minlength-message="This new password is too short. It should have 6 characters or more.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Confirm Password<span class="text text-danger"> *</span></label>
                            <div class="col-lg-8 col-xl-8">
                                <input class="form-control" minlength="6" maxlength="15" placeholder="Enter confirm password" required="required" name="new_password_confirmation" type="password" data-parsley-required-message="Confirm password is required" data-parsley-equalto="#newPassword" data-parsley-minlength-message="This confirm password is too short. It should have 6 characters or more." data-parsley-equalto-message="New password and confirm password should be the same.">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="button" class="btn btn-sm btn-brand btn-square" onclick="updatePassword(this)">Change Password</button>&nbsp;
                    <button type="button" class="btn btn-sm btn-danger btn-square" onclick="cancelForm(this)">Cancel</button>
                </div>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset(mix('js/profile_change_password.js')) }}"></script>
@endsection