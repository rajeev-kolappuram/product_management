@extends('profile::index',['pageTitle'=>'Personal Information','profile_personal_information_active' =>'kt-widget__item--active','profile_title'=>'Personal Information'])
@section('profile_content')
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Personal Information <small>update your personal informaiton</small></h3>
                </div>
            </div>
            <form class="kt-form kt-form--label-right">
                <div class="kt-portlet__body">
                    <div class="kt-section kt-section--first">
                        <div class="kt-section__body">
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input class="form-control" type="text" value="{{ Auth::user()->name ?? '' }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input class="form-control" type="text" value="{{ Auth::user()->role ?? config('app.name').' user' }}">
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                        <input type="text" class="form-control" value="{{ Auth::user()->contact_number ?? '' }}" placeholder="Phone" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                        <input type="text" class="form-control" value="{{ Auth::user()->email ?? '' }}" placeholder="Email" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-sm btn-brand btn-square" onclick="updatePassword(this)">Send Update Request</button>&nbsp;
                                <button type="button" class="btn btn-sm btn-danger btn-square" onclick="cancelForm(this)">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection