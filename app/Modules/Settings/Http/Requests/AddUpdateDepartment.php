<?php

namespace App\Modules\Settings\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddUpdateDepartment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $departmentId = (Request::input('department_id')) ? (Core::decodeId(Request::input('department_id'))) : NULL;
        return [
            'department_name' => ($departmentId) ? ('required|max:255|unique:departments,name,' . $departmentId . ',id') : ('required|max:255|unique:departments,name')
        ];
    }
}
