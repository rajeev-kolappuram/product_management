<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'overview'], function () {
        Route::get('/', 'ProfileOverview@indexAction')->name('profile-overview');
        Route::post('/load-active-logins', 'ProfileOverview@loadActiveLogins')->name('load-active-logins');
        Route::post('/signout-all-sessions', 'ProfileOverview@signoutAllActiveSessions')->name('signout-all-active-sessions');
    });
    Route::group(['prefix' => 'personal-information'], function () {
        Route::get('/', 'ProfilePersonalInformation@indexAction')->name('profile-personal-information');
    });
    Route::group(['prefix' => 'account-information'], function () {
        Route::get('/', 'ProfileAccountInformation@indexAction')->name('profile-account-information');
    });
    Route::group(['prefix' => 'change-password'], function () {
        Route::get('/', 'ProfileController@indexAction')->name('profile-change-password');
        Route::post('/update', 'ProfileController@updatePassword')->name('profile-update-password');
    });
    Route::get('/', 'ProfileOverview@indexAction')->name('profile');
    Route::post('/profile-details', 'ProfileController@profileDetails')->name('profile-details');
});
