<?php

namespace App\Modules\UserManagement\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\UserManagement\Models\Permission;
use App\Modules\UserManagement\Models\Role;

class UserManagement extends Controller
{

    public function index()
    {
        $widgets = [
            'roles' => [
                'name'  => 'Roles',
                'count' => Role::count(),
                'route' => route('user-management-roles')
            ],
            'permissions' => [
                'name'  => 'Permissions',
                'count' => Permission::count(),
                'route' => route('user-management-permissions')
            ]
        ];

        return view('user-management::index')->with('widgets', $widgets);
    }
}
