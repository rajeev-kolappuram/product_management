"use strict";

var KTWizard2 = function () {
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    var initWizard = function () {
        wizard = new KTWizard(wizardEl, { startStep: 1 });
        wizard.on('beforeNext', function (wizardObj) {

            formEl.wrap("<form id='parsley-form'></form>");
            $("#parsley-form").parsley({ excluded: ':hidden' }).validate();
            if (!$("#parsley-form").parsley().isValid()) {
                wizardObj.stop();  // don't go to the next step
            }
            formEl.unwrap();

        })
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }

    var initValidation = function () {



        if (validator != false) {
            formEl.parsley({ excluded: ':hidden' }).validate();
            if (!formEl.parsley().isValid()) {
                validator = false;
            }
        }


    }

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        btn.on('click', function (e) {
            e.preventDefault();
            if (validator.form()) {
                KTApp.progress(btn);
                formEl.ajaxSubmit({
                    success: function () {
                        KTApp.unprogress(btn);
                        swal.fire({
                            "title": "",
                            "text": "The application has been successfully submitted!",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    }
                });
            }
        });
    }

    return {
        // public functions
        init: function () {
            wizardEl = KTUtil.get('kt_wizard_v2');
            formEl = $('.kt-wizard-v2__content[data-ktwizard-state="current"]');
            initWizard();
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizard2.init();
});