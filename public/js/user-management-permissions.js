/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/Modules/UserManagement/Resources/js/permissionManagement.js":
/*!*************************************************************************!*\
  !*** ./app/Modules/UserManagement/Resources/js/permissionManagement.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
if (ability.add_update_permission) {
  openPermissionModal = function openPermissionModal() {
    var permissionId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    resetForm('#permissionAddUpdateForm');

    if (permissionId != '') {
      $.ajax({
        url: URL_PERMISSION_DETAILS,
        type: "POST",
        data: {
          _token: _token,
          permission_id: permissionId
        },
        success: function success(result) {
          if (result.status == 1) {
            var data = result.data;
            $("#addUpdateModalTitle").text('Update Permission');
            $("#permissionId").val(data.en_id);
            $("#permissionDisplayName").val(data.permission);
            if (data.en_parent_permission_id) $('#permissionParent').empty().append("<option value='" + data.en_parent_permission_id + "' selected>" + data.parent_permission + "</option>").trigger('change');
            $("#permissionModal").modal('show');
          } else {
            toast(result.heading, result.msg, 'warning');
            actionButton.removeAttr('disabled');
          }

          loadingHide();
        }
      });
    } else {
      $("#addUpdateModalTitle").text('Add Permission');
      $("#permissionModal").modal('show');
    }
  };
}
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


$("#permissionManagementDataTable").DataTable({
  "pageLength": 25,
  "responsive": true,
  "serverSide": true,
  "ordering": true,
  "aaSorting": [],
  "processing": true,
  "order": [[0, "desc"]],
  "columnDefs": [{
    orderable: false,
    targets: [0, 4]
  }, {
    "width": "5%",
    "targets": 0
  }, {
    "width": "40%",
    "targets": 1
  }, {
    "width": "40%",
    "targets": 2
  }, {
    "width": "8%",
    "targets": 3
  }, {
    "width": "7%",
    "targets": 4
  }],
  "language": {
    "searchPlaceholder": 'Search...',
    "sSearch": '',
    "infoFiltered": " ",
    'loadingRecords': '&nbsp;',
    'processing': dataTableLoader
  },
  "ajax": {
    "url": URL_PERMISSION_LIST,
    "type": "post",
    'data': function data(_data) {
      _data._token = _token;
      return _data;
    }
  },
  "AutoWidth": false,
  "columns": [{
    "data": "en_id"
  }, {
    "data": "permission",
    "name": "permission"
  }, {
    "data": "parent_permission",
    "name": "parent_permission"
  }, {
    "data": "status",
    "name": "status"
  }, {
    "data": "en_id"
  }],
  "fnCreatedRow": function fnCreatedRow(nRow, aData, iDataIndex) {
    loadingShow();
    var info = this.dataTable().api().page.info();
    var page = info.page;
    var length = info.length;
    var index = page * length + (iDataIndex + 1);
    var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.permission) + '","' + aData.status + '")';
    var editFunction = 'javascript:openPermissionModal("' + aData.en_id + '")';

    if (aData.status == 'active') {
      activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>Active</span>";
      activeTitle = 'Inactivate';
      activeIcon = "la la-times-circle";
    } else if (aData.status == 'inactive') {
      activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>Inactive</span>";
      activeTitle = 'Activate';
      activeIcon = "la la-check-circle";
    }

    var htmlData = '';
    htmlData += " <div class=\"dropdown dropdown-inline\">";
    htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
    htmlData += "<i class=\"flaticon-more-1\"></i>";
    htmlData += "</button>";
    htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";
    if (ability.add_update_permission) htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";
    if (ability.change_status_permission) htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";
    htmlData += "</div>";
    htmlData += "</div>";
    $('td:eq(0)', nRow).html(index).addClass('text-center');
    $('td:eq(1)', nRow).html(aData.permission).addClass('text-left');
    $('td:eq(2)', nRow).html(aData.parent_permission).addClass('text-center');
    $('td:eq(3)', nRow).html(activeStatus).addClass('text-center');
    $('td:eq(4)', nRow).html(htmlData).addClass('text-center');
  },
  "fnDrawCallback": function fnDrawCallback(oSettings) {
    var info = this.dataTable().api().page.info();
    var totalRecords = info.recordsDisplay;
    loadPermissionHierarchy();
    loadingHide();
    updateTotalRecordsCount("total-records-permissions", totalRecords);
  }
});
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */

if (ability.change_status_permission) {
  activateDeactivate = function activateDeactivate() {
    var permissionId = $("#activateUpdateId").val();
    $.ajax({
      type: 'POST',
      data: {
        _token: _token,
        permission_id: permissionId
      },
      url: URL_CHANGE_STATUS,
      success: function success(data) {
        if (data.status == 1) {
          sweetalert(data.heading, data.msg, 'success');
          $('#permissionManagementDataTable').DataTable().ajax.reload(null, false);
        } else {
          sweetalert(data.heading, data.msg, 'error');
        }
      }
    });
  };
}
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


$('#permissionParent').select2({
  placeholder: "Select Parent Permission",
  minimumResultsForSearch: minimumResultsForSearch,
  ajax: {
    url: URL_PARENT_PERMISSIONS,
    dataType: 'json',
    data: function data(params) {
      return {
        q: params.term,
        // search term
        page: params.page
      };
    },
    processResults: function processResults(data) {
      return {
        results: $.map(data.items, function (item) {
          return {
            text: item.permission,
            slug: item.permission,
            id: item.en_id
          };
        })
      };
    }
  }
}).change(function () {
  $(this).parsley().validate();
});
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */

loadPermissionHierarchy = function loadPermissionHierarchy() {
  $.ajax({
    type: 'POST',
    data: {
      _token: _token
    },
    url: URL_PERMISSION_HIERARCHY,
    success: function success(data) {
      if (data.status == 1) {
        $("#permissionHierarchy").html(data.html);
        var data = JSON.parse(data.data);
        loadJsTree(data);
      }
    }
  });
};
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */


window.loadJsTree = function (data) {
  $('#permissionHierarchyTree').jstree({
    'plugins': ["wholerow", "types"],
    'core': {
      "themes": {
        "responsive": true
      },
      'data': data
    },
    "types": {
      "default": {
        "icon": "fa fa-folder kt-font-warning"
      },
      "file": {
        "icon": "fa fa-folder  kt-font-warning"
      }
    }
  }).on('loaded.jstree', function () {
    $('#permissionHierarchyTree').jstree('open_all');
  });
};

/***/ }),

/***/ 7:
/*!*******************************************************************************!*\
  !*** multi ./app/Modules/UserManagement/Resources/js/permissionManagement.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ability-shopping/app/Modules/UserManagement/Resources/js/permissionManagement.js */"./app/Modules/UserManagement/Resources/js/permissionManagement.js");


/***/ })

/******/ });