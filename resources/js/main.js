
'use strict'

/**
 * minimizeMenu
 * 
 * @parm
   
 */
window.minimizeMenu = () => {
    if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
        $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
        $('body').addClass('collapsed-menu');
        $('.show-sub + .br-menu-sub').slideUp();
    } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
        $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
        $('body').removeClass('collapsed-menu');
        $('.show-sub + .br-menu-sub').slideDown();
    }
}

minimizeMenu();
$(window).resize(function () {
    minimizeMenu();
});


/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.checkSession = () => {
    $.ajax({
        url: URL_checkSession,
        type: 'POST',
        data: { _token: _token },
        dataType: 'JSON',
        success: function (data) {
            if (data.status == 1) {
                window.location = URL_login;
            }
        },
        error: function (error) {
            window.location = URL_login;
        }
    });
}
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.numberCodeDetailsContact = (parenetInput, hiddenInput) => {
    selectedCountryAssign(parenetInput, hiddenInput);
    var input = document.querySelector(parenetInput);
    input.addEventListener("countrychange", function () {
        selectedCountryAssign(parenetInput, hiddenInput);
    });
}
/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.selectedCountryAssign = (parenetInput, hiddenInput) => {
    var selectedcountry = {
        'dialCode': ($(parenetInput).intlTelInput("getSelectedCountryData").dialCode),
        'iso2': ($(parenetInput).intlTelInput("getSelectedCountryData").iso2),
        'name': $(parenetInput).intlTelInput("getSelectedCountryData").name,
        'number': $(parenetInput).val()
    };
    selectedcountry = JSON.stringify(selectedcountry);
    $(hiddenInput).val(selectedcountry);
}

/**
 * Insert text at cursor position.
 * 
 * @parm
   
 */
window.telInputOptions = {
    utilsScript: URL_INTL_UTILS,
    allowDropdown: true,
    nationalMode: true,
    preferredCountries: ["in"],
    initialCountry: 'in',
    separateDialCode: true
};
$("input[type=tel]").intlTelInput(telInputOptions);

/**
 * @function sideMenuToggle
 * Fucntion to sideMenuToggle
 * 
 * @parm
 * @return
 * 
 * @author Sanith 
 */

$('#kt_aside_toggler').click(function () {
    if ($('#kt_aside_toggler').hasClass('kt-aside__brand-aside-toggler--active')) {
        setCookie('sideMenu', 'small', 180)
    } else {
        setCookie('sideMenu', 'big', 180)
    }
})
/*
  * @function viewUserOpen
  * view user details
  * 
  * @param 
  * @return 
  * 
  * @author
  * Jasin
  */
function viewUserOpen(id) {
    $.ajax({
        type: 'POST',
        data: { _token: _token, id: id },
        url: URL_viewUserDetails,
        success: function (data) {
            if (data.status == 1) {
                $("#generalUserDetails").html(data.data);
                $("#generalUserDetails").modal('show');
            } else {
                toast('Details Not Found', 'warning')
            }
        }

    });
}


/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.target.tagName != 'TEXTAREA') {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        }
    });
});


/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('/');

}


/*
 * @function addslashes
 * 
 * @param 
 * @return 
 * 
 *  @Author Libin
 */
function dateDisplayFormat(date, type = 'date_time') {
    var dateTime = new Date(date);
    if (type == 'date') {
        dateTime = moment(dateTime).format("DD/MM/YYYY");
    } else {
        dateTime = moment(dateTime).format("DD/MM/YYYY hh:mm A");
    }
    return dateTime;

}

/*
 * @function addslashes
 * 
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 *  @Author Libin
 */
function addslashes(str) {
    str = str.replace(/\\/g, '\\\\');
    str = str.replace(/\'/g, '\\\'');
    str = str.replace(/\"/g, '\\"');
    str = str.replace(/\0/g, '\\0');
    return str;
}

function stripslashes(str) {
    str = str.replace(/\\'/g, '\'');
    str = str.replace(/\\"/g, '"');
    str = str.replace(/\\0/g, '\0');
    str = str.replace(/\\\\/g, '\\');
    return str;
}


$('.select-without-search').select2({
    minimumResultsForSearch: 1 / 0
});
/**
 * @function acccept_txtOnly
 * acccept_txtOnly
 * 
 * @parm
 * @return
 * 
 * @author Drishya 
 */
$(".acccept_txtOnly").keydown(function (e) {
    if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
        }
    }
});

/*
 * 
 * Accept Digit Only
 * 
 * @param param 
 * @return null
 * 
 * Akshay
 */
$(".accept_digit_only").keypress(function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
});

/*
 * 
 * accept_alpha_numeric_only
 * 
 * @param param 
 * @return null
 * 
 * Akshay
 */
$(".ano").keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});

/**
 * accept_alpha_numeric_with_whitespace_only
 * 
 * @parm
   
 */
$(".anwwo").keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9 ]*$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});
/*
 * 
 * Accept Digit Only
 * 
 * @param param 
 * @return null
 * 
 * Akshay
 */
$(".pan-card-validation").blur(function (e) {
    if ($(this).val()) {
        var regex = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        var regex = new RegExp(regex);
        var str = $(this).val();
        if (!regex.test(str)) {
            toast('INVALID PANCARD', 'Please enter a valid pan card number', 'error');
            $(this).val('');
        }
    }
});

/*
 * 
 * Accept Digit Only
 * 
 * @param param 
 * @return null
 * 
 * Akshay
 */
$(".reject_digit_only").keypress(function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return true;
    }
    return false;
});
/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
$(".accept_decimal_only").keypress(function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode == 47 || charCode > 57)) {
        return false;
    }
    return true;
});

/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
$('.prevent_paste').bind("cut copy paste", function (e) {
    e.preventDefault();
});


/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
$(".allow_numeric_with_one_dot").on("keypress", function (evt) {
    if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
        $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
/*
 * @function apiTrack
 * apiTrack
 * 
 * @param 
 * 
 * 
 * @return 
 * string
 * 
 * @author
 * Akshay
 */
$(".percentage-mask").on("keyup", function (evt) {
    if ($(this).val() < 100) {
        if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
            $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    } else {
        $(this).val('');
    }
});





$('body').on('shown.bs.modal', '.modal', function () {
    $('input:visible:enabled:first', this).focus();
});

/*
 * @function updateTotalRecordsCount
 * apiTrack
 * 
 * @param 
 * @return 
 * 
 * @author
 * Akshay
 */
window.updateTotalRecordsCount = (classForLael, totalRecords) => {
    $("." + classForLael).html(totalRecords);
}

/*
 * @function jsUcfirst
 * apiTrack
 * 
 * @param 
 * @return 
 * 
 * @author
 * Sanith
 */
function jsUcfirst(string) {
    string = string.trim();
    return string.charAt(0).toUpperCase() + string.slice(1);
}


/*
 * @function jsUcwords
 * jsUcwords
 * 
 * @param 
 * @return 
 * 
 * @author
 * Shiniya M
 */

function jsUcwords(word = '') {
    word = word.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
    return word;
}
/**
 * @function humanize
 * humanize
 * 
 * @parm
 * @return
 * 
   
 */
function humanize(str) {
    str = str.trim();
    var frags = str.split('_');
    for (i = 0; i < frags.length; i++) {
        frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
}

/**
 * @function setCookie
 * Fucntion to setCookie
 * 
 * @parm
 * @return
 * 
 * @author Sanith 
 */
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

/**
 * @function getCookie
 * Fucntion to getCookie
 * 
 * @parm
 * @return
 * 
 * @author Sanith 
 */
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}



/*
 * 
 * Accept percentage
 * 
 * @param param 
 * @return null
 * 
 * Akshay
 */
$(".accept_percentage").blur(function (evt) {
    var x = parseFloat($(this).val());
    if (isNaN(x) || x < 0) {
        $(this).val(0);
    } else if (x > 100) {
        $(this).val(100);
    }
});

/**
 * This function is same as PHP's nl2br() with default parameters.
 *
 * @param {string} str Input text
 * @param {boolean} replaceMode Use replace instead of insert
 * @param {boolean} isXhtml Use XHTML 
 * @return {string} Filtered text
 */
function nl2br(str, replaceMode, isXhtml) {

    var breakTag = (isXhtml) ? '<br />' : '<br>';
    var replaceStr = (replaceMode) ? '$1' + breakTag : '$1' + breakTag + '$2';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, replaceStr);
}


$('.dataTables_length select').css('width', '70px').select2({ minimumResultsForSearch: Infinity });