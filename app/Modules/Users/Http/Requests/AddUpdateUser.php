<?php

namespace App\Modules\Users\Http\Requests;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class AddUpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = (Request::input('user_id')) ? (Core::decodeId(Request::input('user_id'))) : NULL;
        return [
            'first_name'    => ('required|max:255'),
            'last_name'     => ('required|max:255'),
            'employee_id'   => ($userId) ? ('required|max:255|alpha_dash|unique:users,employee_id,' . $userId . ',id') : ('required|max:255|alpha_dash|unique:users,employee_id'),
            'email'         => ($userId) ? ('required|max:255|email|unique:users,email,' . $userId . ',id') : ('required|unique:users,email|max:255|email'),
            'role_id'       => 'required',
            'department_id' => 'required',
            'mobile_number' => ($userId) ? ('required|max:15|unique:users,mobile_number,' . $userId . ',id') : ('required|unique:users,mobile_number|max:15'),
            'zone_ids'       => 'required'
        ];
    }

    public function getValidatorInstance()
    {
        $this->manipulateRequest();
        $this->getInputSource()->replace(Request::all());
        return parent::getValidatorInstance();
    }

    protected function manipulateRequest()
    {
        if ($this->request->get('mobile_number_data')) {
            $mobileNumberData = json_decode($this->request->get('mobile_number_data'));
        }
        request()->merge([
            'name'               => trim(ucwords(Request::input('first_name') . ' ' . Request::input('last_name'))),
            'first_name'         => trim(ucwords(Request::input('first_name'))),
            'last_name'          => trim(ucwords(Request::input('last_name'))),
            'employee_id'        => trim(strtoupper(Request::input('employee_id'))),
            'email'              => trim(Request::input('email')),
            'mobile_number'      => trim(('+' . $mobileNumberData->dialCode . $mobileNumberData->number)),
            'mobile_number_data' => trim((Request::input('mobile_number_data'))),
            'role_id'            => trim((Request::input('role_id')) ? (Core::decodeId(Request::input('role_id'))) : ''),
            'department_id'      => trim((Request::input('department_id')) ? (Core::decodeId(Request::input('department_id'))) : ''),
            'password'           => trim(Hash::make('letmein')),
            'created_by'         => trim(Auth::id()),
            'zone_ids'           => (Request::input('zone_ids')) ? (array_map(function ($zone) {
                return Core::decodeId($zone);
            }, Request::input('zone_ids'))) : '',
        ]);
        request()->offsetUnset('_token');
    }
}
