/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/Modules/Orders/Resources/js/orderManagement.js":
/*!************************************************************!*\
  !*** ./app/Modules/Orders/Resources/js/orderManagement.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
      
     */
$("input[name='toAddress']").click(function () {
  if ($(this).prop("checked")) {
    $("#addressModal").removeClass('d-none');
  } else {
    $("#addressModal").addClass('d-none');
  }
});
/**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
      
     */

$("input[name='toAddress']").on('click', function () {
  var $box = $(this);

  if ($box.is(":checked")) {
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
/**
     * Laravel - A PHP Framework For Web Artisans
     *
     * @package  Laravel
     
     */

$("#orderManagementDataTable").DataTable({
  "pageLength": 25,
  "responsive": true,
  "serverSide": true,
  "ordering": true,
  "aaSorting": [],
  "processing": true,
  "order": [[0, "desc"]],
  "columnDefs": [{
    orderable: false,
    targets: [0, 5]
  }, {
    "width": "5%",
    "targets": 0
  }, {
    "width": "19%",
    "targets": 1
  }, {
    "width": "19%",
    "targets": 2
  }, {
    "width": "19%",
    "targets": 3
  }, {
    "width": "19%",
    "targets": 4
  }, {
    "width": "19%",
    "targets": 5
  }],
  "language": {
    "searchPlaceholder": 'Search...',
    "sSearch": '',
    "infoFiltered": " ",
    'loadingRecords': '&nbsp;',
    'processing': dataTableLoader
  },
  "ajax": {
    "url": URL_ORDER_LIST,
    "type": "POST",
    'data': function data(_data) {
      _data._token = _token;
      return _data;
    }
  },
  "AutoWidth": false,
  "columns": [{
    "data": "en_id"
  }, {
    "data": "order_number",
    "name": "order_number"
  }, {
    "data": "delivery_date",
    "name": "delivery_date"
  }, {
    "data": "amount",
    "name": "amount"
  }, {
    "data": "status",
    "name": "status"
  }, {
    "data": "en_id"
  }],
  "fnCreatedRow": function fnCreatedRow(nRow, aData, iDataIndex) {
    loadingShow();
    var info = this.dataTable().api().page.info();
    var page = info.page;
    var length = info.length;
    var index = page * length + (iDataIndex + 1);
    var statusChangeFunction = 'javascript:changeStatus("' + aData.en_id + '","' + encodeURIComponent(aData.title) + '","' + aData.status + '")';
    var editFunction = 'javascript:openOrderModal("' + aData.en_id + '")';

    if (aData.status == 'pending') {
      activeStatus = "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--square'>pending</span>";
      activeTitle = 'Confirm';
      activeIcon = "la la-times-circle";
    } else if (aData.status == 'confirm') {
      activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>deliverd</span>";
      activeTitle = 'Delivered';
      activeIcon = "la la-times-circle";
    } else if (aData.status == 'delivered') {
      activeStatus = "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--square'>deliverd</span>";
      activeTitle = 'Delivered';
      activeIcon = "la la-times-circle";
    }

    var htmlData = '';
    htmlData += " <div class=\"dropdown dropdown-inline\">";
    htmlData += "<button type=\"button\" class=\"btn btn-outline-hover-info btn-action btn-icon btn-sm btn-icon-md\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
    htmlData += "<i class=\"flaticon-more-1\"></i>";
    htmlData += "</button>";
    htmlData += "<div class=\"dropdown-menu dropdown-menu-right\">";
    htmlData += "<a class=\"dropdown-item\" href=" + editFunction + "><i class=\"la la-edit\"></i> Edit</a>";
    htmlData += "<a class=\"dropdown-item\" href=" + statusChangeFunction + "><i class='" + activeIcon + "'></i> " + activeTitle + "</a>";
    htmlData += "</div>";
    htmlData += "</div>";
    $('td:eq(0)', nRow).html(index).addClass('text-center');
    $('td:eq(1)', nRow).html(aData.order_number).addClass('text-center');
    $('td:eq(2)', nRow).html(aData.delivery_date).addClass('text-center');
    $('td:eq(3)', nRow).html(aData.amount).addClass('text-center');
    $('td:eq(4)', nRow).html(aData.status).addClass('text-center');
    $('td:eq(5)', nRow).html(htmlData).addClass('text-center');
  },
  "fnDrawCallback": function fnDrawCallback(oOrders) {
    var info = this.dataTable().api().page.info();
    var totalRecords = info.recordsDisplay;
    loadingHide();
    updateTotalRecordsCount("total-records-orders", totalRecords);
  }
});

openOrderModal = function openOrderModal() {
  var ordersId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  resetForm('#OrderAddUpdateForm');

  if (ordersId != '') {
    $.ajax({
      url: URL_ORDER_DETAILS,
      type: "POST",
      data: {
        _token: _token,
        orders_id: ordersId
      },
      success: function success(result) {
        console.log(result);

        if (result.status == 1) {
          var data = result.data;
          $("#addUpdateModalTitle").text('Update Product');
          $("#ordersId").val(data.en_id);
          $("#delivery_date").val(data.delivery_date);
          $("#ordersModal").modal('show');
        } else {
          toast(result.heading, result.msg, 'warning');
          actionButton.removeAttr('disabled');
        }

        loadingHide();
      }
    });
  } else {
    $("#addUpdateModalTitle").text('Add Order');
    $("#ordersModal").modal('show');
  }
};

activateDeactivate = function activateDeactivate() {
  var ordersId = $("#activateUpdateId").val();
  $.ajax({
    type: 'POST',
    data: {
      _token: _token,
      orders_id: ordersId
    },
    url: URL_CHANGE_STATUS,
    success: function success(data) {
      if (data.status == 1) {
        sweetalert(data.heading, data.msg, 'success');
        $('#orderManagementDataTable').DataTable().ajax.reload(null, false);
      } else {
        sweetalert(data.heading, data.msg, 'error');
      }
    }
  });
};

/***/ }),

/***/ 14:
/*!******************************************************************!*\
  !*** multi ./app/Modules/Orders/Resources/js/orderManagement.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ability-shopping/app/Modules/Orders/Resources/js/orderManagement.js */"./app/Modules/Orders/Resources/js/orderManagement.js");


/***/ })

/******/ });