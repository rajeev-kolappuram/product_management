<?php

namespace App\Modules\Users\Models;

use App\Modules\Core\Http\Controllers\Core;
use Illuminate\Database\Eloquent\Model;

class UsersZone extends Model
{
    protected $guarded = [];
    public $timestamps = false;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'zone_id'
    ];


    /**
     * Insert text at cursor position.
     * 
     * @parm
       
     */
    public function getEnZoneIdAttribute($value)
    {
        return Core::encodeId($value);
    }
}
