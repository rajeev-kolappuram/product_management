<?php

use App\Modules\Settings\Database\Seeds\PackTypeSeeder;
use App\Modules\Settings\Database\Seeds\DepartmentSeeder;
use App\Modules\Settings\Database\Seeds\DepartmentUpdateSeeder;
use App\Modules\Settings\Database\Seeds\MachineSeriesSeeder;
use App\Modules\Settings\Database\Seeds\ZoneSeeder;
use App\Modules\UserManagement\Database\Seeds\AssignRolePermissions;
use App\Modules\UserManagement\Database\Seeds\PermissionSeeder;
use App\Modules\UserManagement\Database\Seeds\RoleSeeder;
use App\Modules\UserManagement\Database\Seeds\RoleUpdateSeeder;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DepartmentSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            DepartmentUpdateSeeder::class,
            RoleUpdateSeeder::class,
            PermissionSeeder::class,
            UserUpdateSeeder::class,
            ZoneSeeder::class,
            AssignRolePermissions::class

        ]);
    }
}
