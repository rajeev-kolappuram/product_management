<?php

namespace App\Modules\Settings\Http\Controllers;

use App\Exceptions\catchException;
use App\Exceptions\catchPdoException;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\Core;
use App\Modules\Settings\Http\Requests\AddUpdateZone;
use App\Modules\Settings\Models\Zone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Zones extends Controller
{

    public function index()
    {
        return view('settings::zones.index');
    }

    public function addUpdateZone(AddUpdateZone $request)
    {
        try {
            $zoneId = (Request::input('zone_id')) ? (Core::decodeId(Request::input('zone_id'))) : NULL;
            $zone = Request::input('zone');
            $zoneData = [
                'name'        => Request::input('zone_name'),
                'created_by'  => Auth::id()
            ];
            if ($zoneId) {
                $isZoneUpdated = Zone::where('id', $zoneId)->update($zoneData);
                if ($isZoneUpdated) {
                    return response()->json(['status' => 1, 'heading' => 'Zone Updated', 'msg' => $zone  . ' updated succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $zone  . ' was not updated succesfully.']);
            } else {
                $isZoneCreated = Zone::create($zoneData);
                if ($isZoneCreated) {
                    return response()->json(['status' => 1, 'heading' => 'Zone Created', 'msg' =>  $zone  . ' created succesfully.']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => $zone  . ' was not created succesfully.']);
            }
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getZoneList()
    {
        try {
            $limit = (Request::input('length') != '') ? Request::input('length') : 10;
            $offset = (Request::input('start') != '') ? Request::input('start') : 0;
            $search = Request::input('search');
            $order = Request::input('order');
            $columns = Request::input('columns');
            $colName = 'id';
            $sort = 'desc';
            if (isset($order[0]['column']) && isset($order[0]['dir'])) {
                $colNo = $order[0]['column'];
                $sort = $order[0]['dir'];
                if (isset($columns[$colNo]['name'])) {
                    $colName = $columns[$colNo]['name'];
                }
            }
            $filterData = [];
            $result = Zone::getZonesList($limit, $offset, $search, $colName, $sort, $filterData);
            $data = ["iTotalDisplayRecords" => $result['count'], "iTotalRecords" => $limit, "TotalDisplayRecords" => $limit];
            $data['data'] = $result['data'];
            return response()->json($data);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function getZoneDetails()
    {
        try {
            $zoneId = (Request::input('zone_id')) ? (Core::decodeId(Request::input('zone_id'))) : NULL;
            if ($zoneId) {
                $zoneDetails = Zone::getZoneDetails($zoneId);
                if ($zoneDetails) {
                    return response()->json(['status' => 1, 'heading' => 'Zone Details', 'msg' => 'Zone details found successfully', 'data' => $zoneDetails]);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Zone details not found!.', 'data' => []]);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Zone details not found!.', 'data' => []]);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }

    public function changeStatus()
    {
        try {
            $zoneId = (Request::input('zone_id')) ? (Core::decodeId(Request::input('zone_id'))) : NULL;
            if ($zoneId) {
                $zoneDetails = Zone::select('id', 'status')->where('id', $zoneId)->first();
                if ($zoneDetails) {
                    $status = ($zoneDetails['status'] == 'active') ? 'inactive' : 'active';
                    $updateStatus = Zone::where('id', $zoneId)->update(['status' => $status]);
                    if ($updateStatus)
                        return response()->json(['status' => 1, 'heading' => 'Status Updated', 'msg' => 'Zone status updated']);
                    return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Zone status not updated!']);
                }
                return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Zone details not found!']);
            }
            return response()->json(['status' => 0, 'heading' => 'Something Went Wrong', 'msg' => 'Zone details not found!.Please try again.']);
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }


    public function getZoneSelectList()
    {
        try {
            $parameters = collect();
            $parameters['search'] = Request::input('q');
            $zones = Zone::getZoneSelecList($parameters);
            return $zones;
        } catch (\PDOException $e) {
            throw new catchPdoException($e);
        } catch (\Exception $e) {
            throw new catchException($e);
        }
    }
}
